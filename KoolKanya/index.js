/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import RouteComponent from "./src/RouteComponent";

AppRegistry.registerComponent(appName, () => RouteComponent);
