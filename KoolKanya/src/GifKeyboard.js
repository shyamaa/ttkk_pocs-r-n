
import React, {Fragment} from 'react';
//import react in our code.

import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Platform, Alert, Modal, Image,
    ActivityIndicator, StatusBar, Button, Dimensions, TextInput
} from 'react-native';
import GiphySearch from "./GiphySearch";
import searchGifs from "./searchGifs";


const {height, width} = Dimensions.get("window");


export default class GifKeyboardComponent extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            is_gif_modal_visible:false,
            query: '',
            gif_url:''

        }
    }

    componentDidMount(){

        //this.setState({query:'trending'},function () {

            this.searchGifs();
       // })
    }


    openModel=()=>{
        this.setState({is_gif_modal_visible:!this.state.is_gif_modal_visible})
    }

    searchGifs = async () => {
        const { query } = this.state;
        let self=this;
        const search_results = await searchGifs(query);
        this.setState({
            search_results
        },function () {
            if( self.flatlist) {
                self.flatlist.scrollToIndex({index: 0, animated: true});
            }
        });

    }
    getItemLayout = (data, index) => (
        { length: 50, offset: 50 * index, index }
    )

    closeGifModal = (gif_url) => {
        this.setState({
            is_gif_modal_visible: false,
            gif_url
        });
    }

    render(){
        let {is_gif_modal_visible, query,search_results,gif_url}=this.state
        return(
            <View style={{height: height/2,alignItems:'center',marginTop: 50}}>
                <StatusBar barStyle="dark-content" />

                <View style={{height:300,width:300,alignItems:'center'}}>
                {gif_url?<Image source={{uri:gif_url}} style={{height:250,width:250}}/>:null}
                </View>



                <View style={{height: height/2,width:width,}}>
                     {/*   <GiphySearch
                            query={query}
                            onSearch={(query) => {this.setState({ query });this.searchGifs()}}
                            search={this.searchGifs}
                            search_results={search_results}
                            onPick={(gif_url) => this.closeGifModal(gif_url)} />*/}

                    <View style={styles.container}>
                        {search_results  &&
                        <FlatList
                            data={search_results}
                            ref={(ref) => { this.flatlist = ref; }}
                            initialNumToRender={1}
                            extraData={this.state}
                            getItemLayout={this.getItemLayout}
                            removeClippedSubviews={true}
                            keyboardShouldPersistTaps={'handled'}
                            renderItem={({ item }) => {
                                return (
                                    <TouchableOpacity onPress={() => {
                                        this.closeGifModal(item.url);
                                    }}>
                                        <View style={{padding: 4}}>
                                            {console.log(item,'itema')}
                                            <Image
                                                resizeMode={"contain"}
                                                style={styles.image}
                                                source={{uri: item.url}}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                            keyExtractor={(item, index) => item.id}
                            // numColumns={2}
                            horizontal={true}
                            //columnWrapperStyle={styles.list}
                        />
                        }
                        <View style={styles.input_container}>
                            <TextInput
                                style={styles.text_input}
                                onChangeText={(query) => {this.setState({ query });this.searchGifs()}}
                                value={query}
                                placeholder="Search for gifs"
                            />
                        </View>
                    </View>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    custom_actions_container: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    buttonContainer: {
        padding: 10
    },
    modal_body: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 10
    },
    modal_close_container: {
        alignSelf: 'flex-end',
        marginTop: 10,
        marginRight: 10
    },
    modal_close_text: {
        color: '#0366d6'
    },
    input_container: {
        flex: 2
    },
    text_input: {
        height: 35,
        marginTop: 5,
        marginBottom: 10,
        borderColor: "#ccc",
        borderWidth: 1,
        backgroundColor: "#eaeaea",
        margin: 5
    },
    button_container: {
        flex: 1,
        marginTop: 5
    },
    list: {
        justifyContent: 'space-around'
    },
    image: {
        width: 100,
        height: 100
    }
});
