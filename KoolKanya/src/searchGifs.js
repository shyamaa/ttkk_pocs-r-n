const GIPHY_API_KEY = "ubiYQ4HQhSkP9KIwD4takckggpsYRHJy";

const giphy = require('giphy-api')(GIPHY_API_KEY);

const searchGifs = async (query) => {
    let key =query?query:'trending'
    const res = await giphy.search(key);
    console.log(res,'response' );
    const gifs = res.data.map((item) => {
        return {
            id: item.id,
            url: item.images.preview_gif.url
        };
    });
    return gifs;
}

export default searchGifs;
