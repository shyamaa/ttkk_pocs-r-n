
//This is an example of React Native
//FlatList Pagination to Load More Data dynamically - Infinite List
import React, {Component} from 'react';
//import react in our code.

import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Platform,TextInput,Image,
    ActivityIndicator,
} from 'react-native';
import Loader from "./Loader";
import LottieView from "lottie-react-native";
import FeedPost from "./common/FeedPost";
import SearchSuggestion from "./SearchSuggestion";
import _ from "lodash";
//import all the components we are going to use.

const DEFAULT_VALUE="Search User";



export default class BlogsContainer extends Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            //Loading state used while loading the data for the first time
            serverData: [],
            totalCount: '',
            //Data Source for the FlatList
            fetching_from_server: false,
            animating:false,
            //Loading state used while loading more data;
            searchValue:'',
            inFocus:false,
            searchData:[]
        };
        this.onEndReachedCalledDuringMomentum = false;


        this.offset = 0;
        //Index of the offset to load from web API
    }

    _keyExtractor = (item, index) => item.id;


    componentDidMount() {

        let URL = 'https://poc-backend.koolkanya.com/backendPoc/api/v1/user/getAllUsers?pageNo=' + this.offset + ' &count=10&sortType=1&search='
        fetch(URL)
        //Sending the currect offset with get request
            .then(response => response.json())
            .then(responseJson => {
                //Successful response from the API Call
                this.offset = this.offset + 1;
                //After the response increasing the offset for the next API call.
                this.setState({
                    serverData: [...this.state.serverData, ...responseJson.responseData.users],
                    totalCount: responseJson.responseData.Usercount,
                    //adding the new data with old one available in Data Source of the List
                    loading: false,
                    //updating the loading state to false
                });
            })
            .catch(error => {
                console.error(error);
            });
    }

    loadMoreData = () => {
        //On click of Load More button We will call the web API again
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({loading: true})

            let URL = 'https://poc-backend.koolkanya.com/backendPoc/api/v1/user/getAllUsers?pageNo=' + this.offset + ' &count=10&sortType=1&search='

            this.setState({fetching_from_server: true}, () => {
                fetch(URL)
                //Sending the currect offset with get request
                    .then(response => response.json())
                    .then(responseJson => {
                        //Successful response from the API Call
                        this.offset = this.offset + 1;

                        //After the response increasing the offset for the next API call.
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.responseData.users],
                            totalCount: responseJson.responseData.Usercount,
                            //adding the new data with old one available in Data Source of the List
                            fetching_from_server: false,
                            loading: false
                            //updating the loading state to false
                        });
                    })
                    .catch(error => {
                        console.error(error);
                    });
            });
            this.onEndReachedCalledDuringMomentum = true;
        }
    };


    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: '#CED0CE'
                }}
            >
                <ActivityIndicator animating size="large" color={'black'}/>
            </View>
        );
    };


    renderListItem=(index,item)=>{
        let animating;

        return(
            <TouchableOpacity onPress={()=>{animating=true;  setTimeout(() => {animating=false}, 3000);}}>
                <View style={styles.item}>
                    {animating? <LottieView source={require('./assets/9158-confetti.json')}
                                            style={{zIndex:10,height:100}} autoPlay loop />:null}
                    <Text style={styles.text}>
                        {index + 1 + " "}{item.firstName + ' ' + item.lastName}
                    </Text>
                </View>
            </TouchableOpacity>
        )

    }


    //Elastic search

    handleChangeText = (text: string) => {
                let self=this;
        // Recent searches in a later stadium if text.length is 0??
        this.setState({
            searchValue: text
        },function () {

        });
        if (text) {

            let URL = "http://10.10.20.176:3000/backendPoc/api/v1/user/searchUsers?email=" + text.toLowerCase();
            fetch(URL, {
                method: 'get',
                headers: {
                    "Authorization": "Basic YmFja2VuZC1wb2M6YmFja2VuZC1wb2M=",
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            })
                .then(function (response) {
                    return response.json();
                })
                .then(function (res) {
                    let data= Object.values(res.responseData);
                    var arr = _.values(res.responseData);
                    const { responseData } = res;


                    console.log(responseData,"array@@@", 'searchresult1')

                    self.setState({searchData:data},function () {
                        console.log( 'searchresult2',typeof self.state.searchData)

                    })

                });
        }
    }

    handleFocus = () => {
        console.log('focus')

        if (this.state.searchValue === DEFAULT_VALUE) {
            if (this._textInput)
                this._textInput.clear();
            this.setState({
                inFocus: true,
                searchValue: ''
            });
        } else {
            this.setState({
                inFocus: true
            });
        }
        //this.props.onFocusChange(true);
    };

    handleBlur = () => {
        const defaultValue =  DEFAULT_VALUE ;
        console.log('blur')
        this.setState(
            {
                inFocus: false,
            });
    };

    handleItemSelected = (data) => {
        const value = data.name;
        this.setState({
            locationValue: value
        });
        if (this._textInput) this._textInput.blur();
    };

    render() {
        let {serverData, totalCount, fetching_from_server, loading,animating,searchValue, inFocus,searchData} = this.state;

        let loadMoreCallback = () => {
        };
        let feedData;
        if (serverData) {
            feedData = serverData;
            if (serverData.length < totalCount) {
                loadMoreCallback = this.loadMoreData;
            }
        } else {
            feedData = [];
        }

        return (
            <View style={styles.container}>
                {/*{loading ?*/}
                    {/*<ActivityIndicator size="large" color="red"/>*/}
                    {/*: null}*/}
                    {/*<Loader loading={loading}/>*/}

                <View style={styles.inputContainer}>
                    <Image
                        style={styles.icon}
                        source={require('./assets/search.png')}
                    />
                    <TextInput
                        ref={component => {
                            this._textInput = component;
                        }}
                        style={styles.input}
                        placeholder={DEFAULT_VALUE}
                        placeholderTextColor='lightgrey'
                        onChangeText={text => this.handleChangeText(text)}
                        onFocus={() => this.handleFocus()}
                        onBlur={() => this.handleBlur()}
                        value={searchValue}
                    />
                </View>

                {inFocus && (
                    <SearchSuggestion
                        query={searchValue}
                        searchData={this.state.searchData}
                       // onRenderHeaderComponent={this.renderHeaderComponent}
                        onItemSelected={this.handleItemSelected}
                    />
                )}

                <FlatList
                    style={{marginBottom: 10}}
                    data={this.state.serverData}
                    renderItem={({item, index}) => <FeedPost item={item} index={index}/>}
                    removeClippedSubviews
                    //ItemSeparatorComponent={() => <View style={styles.separator} />}
                    ListFooterComponent={this.renderFooter}
                    keyExtractor={this._keyExtractor}
                    onEndReached={loadMoreCallback}
                    onEndReachedThreshold={0.5}
                    onMomentumScrollBegin={() => {
                        this.onEndReachedCalledDuringMomentum = false;
                    }}
                    //Adding Load More button as footer component
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        //backgroundColor: 'white',
        marginTop: 70,
        marginBottom: 50,
    },
    item: {
        margin: 10,
        height: 200,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fdfdff'
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    text: {
        fontSize: 15,
        color: 'black',
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        color: 'white',
        fontSize: 15,
        textAlign: 'center',
    },
    inputContainer: {
        flexDirection: 'row',
        width: '90%',
        //paddingVertical: 10,
        paddingHorizontal: 15,
        height: 38,
        alignSelf: 'center',
        borderWidth:1/2,
        borderRadius:20,
        //margin:5,
        borderColor:'grey'
    },
    input: {
        flex: 1,
        fontFamily: 'Avenir',
        fontSize: 14.0,
        color:'black',
        alignSelf: 'center',

    },
    icon: {
        width: 27.0,
        height: 28.0,
        alignSelf: 'center',
        marginRight: 14,
        marginLeft: 2
    },
});
