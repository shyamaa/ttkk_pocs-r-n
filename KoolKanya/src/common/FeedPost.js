/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, TouchableOpacity, Dimensions,
} from 'react-native';
import LottieView from "lottie-react-native";
const {height, width} = Dimensions.get("window");


class FeedPost extends React.Component {
    constructor(props){
        super(props);
        this.state={
            animating:false
        }
    }

    startAnimation=()=>{
        this.setState({animating:true},function () {
            setTimeout(() => {
                    this.setState({animating:false});
                }, 1000);
        })
    }


    render() {
        let {animating}=this.state;
        let {index,item}=this.props

        return     (

            <TouchableOpacity onPress={()=>this.startAnimation()}>


                <View style={styles.item}>
                    <View style={{height:20, alignItems: 'center',
                        justifyContent: 'center',}}>
                    <Text style={styles.text}>
                        {index + 1 + " "}{item.firstName + ' ' + item.lastName}
                    </Text>
                    </View>

                    {animating? <LottieView source={require('../assets/8307-love-icon-animation.json')}
                                            style={{position: 'absolute',zIndex:10,height:400,width:width}} autoPlay loop />:null}

                </View>
            </TouchableOpacity>
        )

    }
}

const styles = StyleSheet.create({
    item: {
        margin: 10,
        height: 200,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fdfdff',
        borderWidth:1/2,
        marginTop:5,
        //overflow:'hidden'
    },
    separator: {
        height: 0.5,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    text: {
        fontSize: 15,
        color: 'black',
    },
});

export default FeedPost;
