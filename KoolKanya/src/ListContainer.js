import React, {Fragment} from 'react';
//import react in our code.

import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Platform,
    ActivityIndicator, StatusBar,
} from 'react-native';
import BlogsContainer from "./BlogsContainer";


export default class ListContainer extends React.Component{


    render(){
        return(
            <Fragment>
                <StatusBar barStyle="dark-content" />

                <BlogsContainer/>
            </Fragment>
        )
    }

}
