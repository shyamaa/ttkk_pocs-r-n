import React, { Fragment } from 'react';
import { View, FlatList, TextInput, Button, TouchableOpacity, Image, StyleSheet } from 'react-native';

const GiphySearch = ({ query, onSearch, search, search_results, onPick }) => {

    return (
        <Fragment>
            <View style={styles.container}>
                {search_results  &&
                <FlatList
                    data={search_results}
                    initialNumToRender={1}
                    extraData={this.state}
                    keyboardShouldPersistTaps={'handled'}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity onPress={() => {
                                onPick(item.url);
                            }}>
                                <View style={{padding: 4}}>
                                    <Image
                                        resizeMode={"contain"}
                                        style={styles.image}
                                        source={{uri: item.url}}
                                    />
                                </View>
                            </TouchableOpacity>
                        );
                    }}
                    keyExtractor={(item, index) => item.id}
                    // numColumns={2}
                    horizontal={true}
                    //columnWrapperStyle={styles.list}
                />
                }
                <View style={styles.input_container}>
                    <TextInput
                        style={styles.text_input}
                        onChangeText={onSearch}
                        value={query}
                        placeholder="Search for gifs"
                    />
                </View>
            </View>

        </Fragment>
    );
}
//

const styles = StyleSheet.create({
    container: {
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        marginTop: 0
    },
    input_container: {
        flex: 2
    },
    text_input: {
        height: 35,
        marginTop: 5,
        marginBottom: 10,
        borderColor: "#ccc",
        borderWidth: 1,
        backgroundColor: "#eaeaea",
        margin: 5
    },
    button_container: {
        flex: 1,
        marginTop: 5
    },
    list: {
        justifyContent: 'space-around'
    },
    image: {
        width: 100,
        height: 100
    }
});

export default GiphySearch;
