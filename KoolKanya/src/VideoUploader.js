// import React, { Component } from 'react';
// import {View, StyleSheet, Dimensions,Image} from 'react-native';
// import { VideoPlayer,Trimmer } from 'react-native-video-processing';
// const {height, width} = Dimensions.get("window");
//
//
// export default class VideoUploaderComponent extends Component {
//     trimVideo() {
//         const options = {
//             startTime: 0,
//             endTime: 15,
//             quality: VideoPlayer.Constants.quality.QUALITY_1280x720, // iOS only
//             saveToCameraRoll: true, // default is false // iOS only
//             saveWithCurrentDate: true, // default is false // iOS only
//         };
//         this.videoPlayerRef.trim(options)
//             .then((newSource) => console.log(newSource))
//             .catch(console.warn);
//     }
//
//     compressVideo() {
//         const options = {
//             width: 720,
//             height: 1280,
//             bitrateMultiplier: 3,
//             saveToCameraRoll: true, // default is false, iOS only
//             saveWithCurrentDate: true, // default is false, iOS only
//             minimumBitrate: 300000,
//             removeAudio: true, // default is false
//         };
//         this.videoPlayerRef.compress(options)
//             .then((newSource) => console.log(newSource))
//             .catch(console.warn);
//     }
//
//     getPreviewImageForSecond(second) {
//         const maximumSize = { width: 640, height: 1024 }; // default is { width: 1080, height: 1080 } iOS only
//         this.videoPlayerRef.getPreviewForSecond(second, maximumSize) // maximumSize is iOS only
//             .then((base64String) => console.log('This is BASE64 of image', base64String))
//             .catch(console.warn);
//     }
//
//     getVideoInfo() {
//         this.videoPlayerRef.getVideoInfo()
//             .then((info) => console.log(info))
//             .catch(console.warn);
//     }
//
//     whenClicked=(item)=>{
//         console.log(item,'tttttt')
//
//     }
//
//     render() {
//         return (
//             <View style={{ flex: 1 }}>
//                 {/*<VideoPlayer*/}
//                     {/*//ref={ref => this.videoPlayerRef = ref}*/}
//                     {/*startTime={30}  // seconds*/}
//                     {/*endTime={120}   // seconds*/}
//                     {/*play={true}     // default false*/}
//                    {/*// replay={true}   // should player play video again if it's ended*/}
//                     {/*rotate={true}   // use this prop to rotate video if it captured in landscape mode iOS only*/}
//                     {/*source={{ uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" }}*/}
//                     {/*playerWidth={300} // iOS only*/}
//                     {/*playerHeight={500} // iOS only*/}
//                     {/*style={{ backgroundColor: 'black' }}*/}
//                     {/*//resizeMode={VideoPlayer.Constants.resizeMode.CONTAIN}*/}
//                     {/*onChange={({ nativeEvent }) => console.log({ nativeEvent })} // get Current time on every second*/}
//                 {/*/>*/}
//
//                 <VideoPlayer
//                     ref={component => {
//                         this._videoPlayer = component;
//                     }}
//                     startTime={0}
//                     endTime={60}
//                     play
//                     replay={true}
//                     rotate={false}
//                     playerWidth={width}
//                     resizeMode={VideoPlayer.Constants.resizeMode.CONTAIN}
//                     playerHeight={height/2}
//                     style={ { backgroundColor: 'black' }}
//                     source="https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
//                     onChange={({ nativeEvent }) => console.log(nativeEvent,'nativeEvent')}
//                 />
//
//
//
//                {/* <Trimmer
//                     source={"http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"}
//                     height={100}
//                     width={300}
//                     onTrackerMove={(e) => console.log(e.currentTime)} // iOS only
//                    // currentTime={this.video.currentTime} // use this prop to set tracker position iOS only
//                     themeColor={'white'} // iOS only
//                     thumbWidth={30} // iOS only
//                     trackerColor={'green'} // iOS only
//                     onChange={(e) => console.log(e.startTime, e.endTime)}
//                 />*/}
//             </View>
//         );
//     }
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: "#FAFAFA",
//         alignItems: 'center',
//         justifyContent: "center"
//     },
// });




import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image,Dimensions,Platform } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
//import { VideoPlayer,Trimmer } from 'react-native-video-processing';
import Video from 'react-native-video';

const {height, width} = Dimensions.get("window");
let API_KEY='712314971661793';
let API_SECRET='r88yTH5GzEU1vSEL_GwcoxQHEhE';
let CLOUD_NAME='koolkanya-com';


// Add your Cloudinary name here
const YOUR_CLOUDINARY_NAME = "your_cloudinary_name"

// If you dont't hacve a preset id, head over to cloudinary and create a preset, and add the id below
const YOUR_CLOUDINARY_PRESET = "your_cloudinary_preset"

export default class  VideoUploader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            avatarSource: null,
            uploadingImage: false,
            actualPath:''
        }
        this.submit = this.submit.bind(this)
    }

    submit () {
        var options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'video'
            }
        };



        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = {uri: response.uri};
                this.setState({
                    uploadingImg: true
                });
                //console.log(response,'response')
                if (response) {
                    uploadFile(response)
                        .then(response => response.json())
                        .then(result => {

                            console.log(result, 'downloading url');
                            this.setState({
                                avatarSource: {uri: result.secure_url},
                                uploadingImg: false
                            });
                        })
                }
            }
        });

    }


    pickVideo=()=>{
        const options2 = {
            title: 'Select video',
            mediaType: 'video',
            path:'video',
            quality: 1
        };

        ImagePicker.showImagePicker(options2, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = {uri: response.path};
                this.setState({
                    uploadingImg: true,
                    //actualPath:response.uri
                });
                console.log(response,'response')
                if (response) {
                    uploadVideo(response)
                        .then(response => response.json())
                        .then(result => {

                            console.log(result, 'downloading url');
                            this.setState({
                                actualPath:  result.eager[0].secure_url,
                                uploadingImg: false
                            });
                        })
                }


            }
        });




    }

    render() {
        let {actualPath}=this.state;
        return (
            <View style={style.container}>
                <Text style={style.header}>React Native Video Upload with Cloudinary!</Text>
                {
                    this.state.uploadingImg ?
                        <Text style={{marginTop:30}}>Uploading...</Text> :
                        <TouchableOpacity onPress={this.pickVideo} style={style.imgBtn}>
                            {
                                this.state.avatarSource ?
                                    <Image source={this.state.avatarSource} style={style.image} /> :
                                    null
                            }
                        </TouchableOpacity>

                }
                                 {/*  <VideoPlayer
                                        ref={component => {
                                            this._videoPlayer = component;
                                        }}
                                        startTime={0}
                                        endTime={60}
                                        play
                                        replay={true}
                                        rotate={false}
                                        playerWidth={width}
                                        //resizeMode={VideoPlayer.Constants.resizeMode.CONTAIN}
                                        playerHeight={height/2}
                                        style={ { backgroundColor: 'black' }}
                                        source={actualPath}
                                        onChange={({ nativeEvent }) => console.log(nativeEvent,'nativeEvent')}
                                    />*/}

                {console.log('actualPath',actualPath)}


                <View style={{alignItems: 'center',justifyContent:'center'}}>

                {actualPath?
                    <Video source={{ uri: actualPath}}   // Can be a URL or a local file.
                       ref={(ref) => {
                           this.player = ref
                       }}                                      // Store reference
                      // onBuffer={this.onBuffer}                // Callback when remote video is buffering
                           repeat
                           resizeMode={"cover"}
                       onError={(e)=>console.log(e)}               // Callback when video cannot be loaded
                       style={{height:height/2,width:width,alignSelf:'center'}} />:null}
                </View>

                {/*{actualPath?<View> <Trimmer*/}
                    {/*source={actualPath}*/}
                    {/*height={100}*/}
                    {/*width={300}*/}
                    {/*onTrackerMove={(e) => console.log(e.currentTime)} // iOS only*/}
                    {/*// currentTime={this.video.currentTime} // use this prop to set tracker position iOS only*/}
                    {/*themeColor={'white'} // iOS only*/}
                    {/*thumbWidth={30} // iOS only*/}
                    {/*trackerColor={'green'} // iOS only*/}
                    {/*onChange={(e) => console.log(e.startTime, e.endTime)}*/}
                {/*/>*/}
                {/*</View>:null}*/}
            </View>
        )
    }
}

function uploadFile(file) {

    return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/koolkanya-com/image/upload?upload_preset=' + 'koolkanya',
        {'Content-Type': 'multipart/form-data'},
        [{ name: 'file', filename: file.fileName, data: RNFetchBlob.wrap(file.path) }]
    )
}

function uploadVideo(file) {
                //file.uri working on android real device
    // file.origURL on ios simulator

    return RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/koolkanya-com/video/upload?upload_preset=' + 'koolkanya',
        {'Content-Type': 'multipart/form-data'},
        [{ name: 'file', filename: "video", data:Platform.OS === 'ios'? RNFetchBlob.wrap(file.origURL):RNFetchBlob.wrap(file.uri)}]
    )
}

const style = StyleSheet.create({
    header: {
        position: 'relative',
        top: 20,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    container: {
        //flex: 1,
        marginTop:70,
        backgroundColor: "#FAFAFA",
        alignItems: 'center',
        //justifyContent: "center"
    },
    imgBtn: {
        height: 80,
        width: 80,
        marginTop:30,
        borderRadius: 40,
        backgroundColor: "#333",
        marginBottom: 10
    },
    image: {
        height: 80,
        width: 80,
        borderRadius: 40
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
})




