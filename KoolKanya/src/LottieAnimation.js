/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import LottieView from 'lottie-react-native';


class LottieAnimation extends React.Component {


    render() {
        // return (
        //     <View style={styles.container}>
        //         <StatusBar barStyle="dark-content"/>
        //
        //         <LottieView source={require('./assets/9158-confetti.json')} autoPlay loop />;
        //
        //
        //     </View>
        // );
        return     (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <LottieView source={require('./assets/4770-lady-and-dove.json')} autoPlay loop />
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FAFAFA",
        alignItems: 'center',
        justifyContent: "center"
    },
});

export default LottieAnimation;
