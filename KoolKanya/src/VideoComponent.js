
import React, { Component } from 'react'
import {StyleSheet, View, Text, TouchableOpacity, Image, Platform, Dimensions} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import {Actions} from 'react-native-router-flux';
const {height, width} = Dimensions.get("window");


let API_KEY='712314971661793';
let API_SECRET='r88yTH5GzEU1vSEL_GwcoxQHEhE';
let CLOUD_NAME='koolkanya-com';


// Add your Cloudinary name here
const YOUR_CLOUDINARY_NAME = "your_cloudinary_name"

// If you dont't hacve a preset id, head over to cloudinary and create a preset, and add the id below
const YOUR_CLOUDINARY_PRESET = "your_cloudinary_preset"

export default class  VideoComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            avatarSource: null,
            uploadingImage: false
        }
        this.submit = this.submit.bind(this)
    }

    submit () {
        var options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'image'
            }
        };

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = {uri: response.uri};
                this.setState({
                    uploadingImg: true
                });
               // console.log(response,'response')
                if (response) {
                    uploadFile(response)
                        .then(response => response.json())
                        .then(result => {

                            console.log(result, 'downloading url');
                            this.setState({
                                avatarSource: {uri: result.eager[0].secure_url},
                                uploadingImg: false
                            });
                        })
                }
            }
        });

    }

    render() {
        return (
            <View style={style.container}>
                <Text style={style.header}>React Native Image Upload with Cloudinary!</Text>
                {
                    this.state.uploadingImg ?
                        <Text>Uploading...</Text> :
                        <TouchableOpacity onPress={this.submit} style={style.imgBtn}>
                            {
                                this.state.avatarSource ?
                                    <Image source={this.state.avatarSource} style={style.image} /> :
                                    null
                            }
                        </TouchableOpacity>
                }
                <TouchableOpacity onPress={()=>{Actions.push('editor')}}>
                    <Text>Custom Editer Screen</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

function uploadFile(file) {

    //console.log(file,'responseresponse');
    //{ cloud_name: "demo", width: 200, crop: "scale"}
    //{name: 'upload_preset', data: '<upload_preset>'}
    //file.path is working on android real device
    //file.orgiUrl working on ios simulator

    return RNFetchBlob
        .fetch('POST', 'https://api.cloudinary.com/v1_1/koolkanya-com/image/upload?upload_preset=' + 'ml_default',
        {'Content-Type': 'multipart/form-data'},
        [{ name: 'file', filename: file.fileName, data:Platform.OS === 'ios'? RNFetchBlob.wrap(file.origURL):RNFetchBlob.wrap(file.path)}]
    )
}

const style = StyleSheet.create({
    header: {
        position: 'relative',
        top: -70,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    container: {
        flex: 1,
        backgroundColor: "#FAFAFA",
        alignItems: 'center',
        justifyContent: "center"
    },
    imgBtn: {
        height: 80,
        width: 80,
        borderRadius: 40,
        backgroundColor: "#333",
        marginBottom: 20
    },
    image: {
        height: height/3,
        width: width,
        alignSelf:'center'
       // borderRadius: 40
    }
})




