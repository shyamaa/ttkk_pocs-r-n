/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList} from 'react-native';
import {Route, Router, Scene} from 'react-native-router-flux';
import App from "../App";
import VideoComponent from "./VideoComponent";
import BlogsContainer from "./BlogsContainer";
import ListContainer from "./ListContainer";
import VideoUploaderComponent from "./VideoUploader";
import GifKeyboardComponent from "./GifKeyboard";
import LottieAnimation from "./LottieAnimation";
import CustomEditer from "./CustomEditer";


export default class RouteComponent extends Component<Props> {


    render() {
        return (
            <Router>
                <Scene key="root" hideNavBar >
                <Scene key="main" tabs={true} swipeEnabled={false} tabBarPosition={'bottom'}
                       tabBarStyle={styles.tabBar} activeTintColor={'#dde0e0'}
                       showLabel={true} default="tab1">
                    <Scene
                        key="tab1"
                        title="Blog"
                        hideNavBar={true}
                        initial
                        component={App}/>
                    <Scene
                        key="tab2"
                        title="List"
                        hideNavBar={true}
                        component={ListContainer}/>

                    <Scene
                        key="tab3"
                        title="Image"
                        hideNavBar={true}
                        component={VideoComponent}/>

                    <Scene
                        key="tab4"
                        title="Video"
                        hideNavBar={true}
                        component={VideoUploaderComponent}/>

                    <Scene
                        key="tab5"
                        title="GIF"
                        hideNavBar={true}
                        component={GifKeyboardComponent}/>
                    <Scene
                        key="tab6"
                        title="Lottie"
                        hideNavBar={true}
                        component={LottieAnimation}/>
                </Scene>



                    <Scene
                        key="editor"
                        title="Editer"
                        hideNavBar={false}
                        component={CustomEditer}/>



                </Scene>
            </Router>
        );
    }
}


const styles = StyleSheet.create({
        container: {
        flex: 1,
    },
        tabBar: {
        backgroundColor: '#6610f2',
       // opacity:.8,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,

    },
        icon: {
            width: 18,
        height: 22,
        padding: 5
    },
        bigicon: {
        width: 25,
        height: 29,
    },
    });
