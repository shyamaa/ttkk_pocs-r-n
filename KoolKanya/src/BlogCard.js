/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, Dimensions, Platform, ActivityIndicator
} from 'react-native';
import {WebView} from 'react-native-webview';
//import HTML from 'react-native-render-html';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import KKHtml from "./assets/helperHTML";

const {height, width} = Dimensions.get("window");


class BlogCard extends React.Component {
    constructor(prop) {
        super(prop);
        this.state = {
            content: '',
            title: '',
            postData: '',
            loading:true
        }
    }

    componentDidMount() {
        console.disableYellowBox = true

        //fetching Api data

        let self = this;
        let URL = 'https://koolkanya.com/blogs/wp-json/wp/v2/posts/1335'
        fetch(URL, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (res) {
                //let data=res.json();

                console.log(res, 'response');
                console.log(res.content, 'response');

                //attaching styling head in api rendering data

                let content=KKHtml +res.content.rendered+'</body></html>'

                self.setState({content: content})
                self.setState({title: res.title.rendered})
                self.setState({postData: res})

            })
            .then(obj => {

            })

    }

    loadingStatusUpdate=()=>{
        this.setState({loading:!this.state.loading})
    }

    render() {
        const {content, title, postData,loading} = this.state;
        const htmlStyles = { p: {fontFamily: 'Avenir'} }
        const htmlContent = `<H1>My Html</H1>`;
        return (
            <View style={{flex: 1, marginTop:40,margin:5,marginBottom:30}}>

                {/*<HTML html={termsHtml} imagesMaxWidth={Dimensions.get('window').width} />


                <HTML html={JqueryHtml} imagesMaxWidth={Dimensions.get('window').width}/>

                <Text style={styles.sectionTitle}>{title}</Text>

                <HTML html={`${content}`} imagesMaxWidth={Dimensions.get('window').width}/>

                {postData?<HTML uri={postData.guid.rendered} imagesMaxWidth={Dimensions.get('window').width}/>:null}
                {console.log(postData,'postData')}*/}

                {/*Web View Approach*/}
                {loading?<View
                    style={{
                        paddingVertical: 20,
                        borderTopWidth: 1,
                        borderColor: '#CED0CE'
                    }}>
                    <ActivityIndicator size="large" color="black"/>
                </View>:null}


                {postData?<WebView
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,}}
                    startInLoadingState={false}
                    onLoadStart={this.loadingStatusUpdate}
                    onLoadEnd={()=>console.log('loading end')}
                    javaScriptEnabled={true}
                    scalesPageToFit={(Platform.OS === 'ios') ? false : true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    javaScriptEnabledAndroid={true}
                    // html={`${content}`}
                    source={{html:`${content}`}}
                    //source={{ uri:postData.guid.rendered }}
                />:null}


                {/* <HTML containerStyle={ {margin: 16} }
                      html={ `${content}` }
                      tagsStyles={ htmlStyles } />*/}

            </View>
        );
    }
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default BlogCard;
