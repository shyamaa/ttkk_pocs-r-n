const termsHtml = `
<html style="font-size: 16px;">
    <p>
        <strong>KoolKanya TERMS AND CONDITIONS</strong>
    </p>
    <p>
        Last updated: September 2017
    </p>
    <p>
        By clicking on the "I accept" button or otherwise installing the KoolKanya
        software application on Your device, You are deemed to have reviewed and
        agreed to the terms and conditions of use and access of the service
        provided by KoolKanya. If You do not agree to these Terms of Use, please do
        not access and use the Portal and We suggest that You uninstall the
        KoolKanya software application from Your device.
    </p>
    <p>
        In these Terms of Use, the following have their respective meanings:
    </p>
    <p>
        "<strong>App</strong>" means the KoolKanya software application that You
        have downloaded to Your device and which enables You to access and use the
        Portal to arrange for the provision of care services to the Client, provide
        feedback to KoolKanya in relation to the care services provided and to
        access information and materials in relation to the Client and the Client's
        care services.
    </p>
    <p>
        "<strong>Approved Provider</strong>" means the entity that has developed
        the Care Plan with the Client and is responsible for the delivery of the
        Home Care Package to the Client.
    </p>
    <p>
        "<strong>Care Plan</strong>" means the care plan for the Client that sets
        out the care and services the Client receives that has been developed in
        consultation with the Client by the Approved Provider and is thereafter
        updated from time to time.
    </p>
    <p>
        "<strong>Care Worker</strong>" means a person employed by KoolKanya or
        engaged by KoolKanya who provides care services to the Client.
    </p>
    <p>
        "<strong>Client</strong>" means an individual who has a Home Care Package
        with an Approved Provider and who wishes to receive care from KoolKanya, as
        part of the Home Care Package or an individual who does not have a Home
        Care Package with an Approved Provider but wishes to receive care from
        KoolKanya in exchange for paying KoolKanya directly for the care received.
        In some situations, You and the Client will be the same person; in other
        situations, You may be a Client Advocate for the Client and therefore given
        the right to administer the Client's account with Us, or You may be a User
        and therefore granted access to information held by KoolKanya about the
        Client and the services provided to the Client.
    </p>
    <p>
        "<strong>Client Advocate</strong>" means a User authorised by a Client
        through the App and/or the Portal to administer the Client's account with
        KoolKanya and who has the authority and power to act on behalf of the
        Client until such time as the Client revokes the Client Advocate's
        authorisation.
    </p>
    <p>
        "<strong>GST</strong>" has the same meaning as defined in the GST Law and
        also includes penalties and interest.
    </p>
    <p>
    "<strong>GST Act</strong>" means the    <em>A New Tax System (Goods and Services Tax) Act 1999</em> (Cth) as
        amended or replaced from time to time.
    </p>
    <p>
        "<strong>GST Law</strong>" has the same meaning as defined in the GST Act.
    </p>
    <p>
        Any expression used that is defined in the GST Law has that defined meaning
        where the context permits.
    </p>
    <p>
        "<strong>Home</strong>" means, in relation to the Client, the Client's
        primary place of residence and the location at which the Client will
        receive the care services from a Care Worker.
    </p>
    <p>
        "<strong>Home Care Package</strong>" means a package of services which the
        Approved Provider has agreed to provide to the Client.
    </p>
    <p>
        "<strong>IPR</strong>" means any copyright, trade mark (whether registered
        or unregistered), design, patent, semiconductor or circuit layout rights,
        trade, business or company names, other proprietary rights and rights to
        the registration of such rights existing anywhere in the world and existing
        before, on or after the commencement of these Terms of Use.
    </p>
    <p>
        "<strong>Laws</strong>" means all relevant state, territory and
        Commonwealth laws and regulations and where the context requires, includes
        all Australian Government policies applicable to the delivery of the Home
        Care Package and the care services to the Client.
    </p>
    <p>
        "<strong>Password</strong>" means a password You set and use for the
        purpose of accessing the Portal and the restricted features of the App.
    </p>
    <p>
        "<strong>Personal Information</strong>" has the meaning given to the term
        "personal information" by the <em>Privacy Act 1988</em> (Cth).
    </p>
    <p>
        "<strong>Portal</strong>" means the KoolKanya portal accessible via the App
        and accessible online through Our website which provides Clients with the
        capacity to arrange and book appointments for the provision of care
        services by a Care Worker consistent with the Client's Care Plan and Home
        Care Package, to manage the payment of Fees to KoolKanya as consideration
        for the provision of care services, to provide feedback in relation to the
        provision of care to the Client and to set access restrictions in relation
        to the right of a User to access and review feedback and notes regarding
        the care services provided to the Client.
    </p>
    <p>
        "<strong>Service Schedule</strong>" means the agreed schedule for the
        delivery of the care services under the Client's Care Plan as updated from
        time to time.
    </p>
    <p>
        "<strong>User</strong>" means a third party individual who has created an
        account with Us in order to access the Portal and has limited or restricted
        right of access to the feedback and notes in relation to the provision of
        care to the Client (such access right is set by the Client using the tools
        We make available through the App).
    </p>
    <p>
        '<strong>User ID</strong>" means a name or acronym which You generate and
        which You use to access and use the App and the Portal.
    </p>
    <p>
        <strong>Terms and Conditions</strong>
    </p>
    <ol>
        <li>
            <strong></strong>
            <strong>Access and Use of the App and the Portal</strong>
        </li>
        <ol>
            <li>
                In order to access some features We make available via the App and
                the Portal, You acknowledge and agree that You will have to create
                an account with Us (by generating a User ID and a Password).
            </li>
            <li>
                When creating Your account, You must provide (and You represent and
                warrant to Us that You have provided) accurate and complete
                information to Us. If You fail to provide accurate and complete
                information, then We may be unable to provide the Portal, the App
                and the services that We make available to You through the App and
                Portal.
            </li>
            <li>
                In addition to the provisions set out in clause 2 (Becoming a
                Client), You acknowledge and agree that We may assist in the
                creation of an account for You when You are creating an account as
                a Client (and therefore recipient of care services from Us). Where
                We provide assistance in the creation of an account, You agree that
                the information You provided to us in the course of creating Your
                account was true, complete and accurate, and not misleading.
            </li>
            <li>
                Where You are creating an account with Us as a User, You
                acknowledge and agree that You have been invited to create an
                account by the Client or by the Client Advocate to create an
                account, solely for the purpose of accessing information and
                records We store about the Client.
            </li>
            <li>
                If at any stage during Your use of the App and the Portal (and the
                related services We provide to You), You determine that the
                information which You have provided to Us is inaccurate and/or
                incomplete, then You must promptly notify Us and update the
                information which You have provided to Us.
            </li>
            <li>
                You are solely responsible for the activity that occurs on Your
                account, and You agree to ensure that only You use your User ID and
                Password to access the restricted areas through the App and on the
                Portal. You agree that You will immediately notify Us if You become
                aware of Your User ID and Password being used to access the Portal
                and the App without Your consent or that You become aware of or
                suspect that Your account has been accessed without Your consent.
            </li>
            <li>
                You also agree that You access and use the App and the Portal only
                in accordance with these Terms of Use, any additional requirements
                specified on the App and the Portal, any lawful and reasonable
                directions given by Us or on Our behalf from time to time and in
                accordance with all applicable laws.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Becoming a Client:</strong>
        </li>
        <ol>
            <li>
                You may become a Client at any time, in accordance with these Terms
                of Use.
            </li>
            <li>
                When You wish to become a Client, You agree that We may provide
                assistance to You to become a Client and You agree to comply with
                Our reasonable instructions (as set out on the App and on the
                Portal or as otherwise communicated by Us to You) and provide Us
                with all necessary information We require to establish You as a
                Client.
            </li>
            <li>
                In particular, You agree to provide Us with information We request
                in relation to Your Approved Provider, Your Care Plan and Your Home
                Care Package, to the extent We (on reasonable grounds) request this
                information. You also authorise Us to liaise with Your Approved
                Provider in relation to the care services that are being or will be
                provided to You under Your Care Plan.
            </li>
            <li>
                Additionally, if We require that You pay any amount in
                consideration of You becoming a Client and having the access and
                use rights We confer on all Clients, then You agree to pay (and You
                will promptly pay) to Us without set-off such consideration
                (including, where relevant, any goods and services tax, consumption
                tax or other value added tax, as may be applicable in the
                circumstances to the consideration) as is applicable.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>
                Ability of Clients and Client Advocates to nominate Users, etc.
            </strong>
        </li>
        <ol>
            <li>
                As a Client You are entitled to nominate any User to have access to
                the feedback and materials We hold and decide to make available
                about the care You receive from Us (including from the Care
                Givers). You acknowledge and agree that We act only on Your
                instructions in relation to the nomination and that You hold Us
                harmless from and against any loss You suffer as a result of any
                information We make available or to which We allow access about You
                or the care services provided to You, unless such loss is directly
                caused by Our negligent or wilful act or omission or Our breach of
                these Terms of Use. You further acknowledge and agree that We will
                maintain the confidentiality and privacy of records of the care You
                receive from Us and, except as required by law, We will not
                disclose such records to any individual.
            </li>
            <li>
                Where You are a Client or a Client Advocate, You agree that You are
                entitled to change the level of access granted to any User by
                accessing and using the tools We make available via the App and the
                Portal and the assistance We otherwise provide or make available.
            </li>
            <li>
                In addition to nominating Users, You may at any time (via the tools
                We make available via the App and the Portal) nominate a User to be
                Your Client Advocate. By nominating a User to be Your Client
                Advocate, You represent and undertake to Us that You have
                authorised the User to act on Your behalf in relation to the care
                services We provide to You and the administration of Your account
                with Us.
            </li>
            <li>
                You acknowledge and agree that where You have authorised a User to
                be Your Client Advocate, We may directly liaise with Your Client
                Advocate in relation to any matter or issue arising under these
                Terms of Use, including in relation to the delivery of care
                services to You and payment of fees and charges to Us in
                consideration for the delivery of care services.
            </li>
            <li>
                You may at any time change or revoke the authorisation of the
                nominated User to be Your Client Advocate by contacting Us
                directly. Please note that We reserve the right to verify Your
                identity and please allow at least 48 hours for any revocation of
                authority to take effect.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Delivery of Care Services:</strong>
        </li>
        <ol>
            <li>
                Where You, as a Client, either directly or through Your Client
                Advocate, arrange and book for the provision of care services by a
                Care Worker, You and Us must comply with the provisions set out in
                this clause which should be read in conjunction with Our right to
                suspend the provision of the care services to You as set out in
                clause 5 (Suspension of the provision of care services).
            </li>
            <li>
                Without limiting the foregoing, You acknowledge and agree that We
                may refuse to provide care services to You where the provision of
                care services has been booked to fall on a public holiday in the
                jurisdiction in which You are located.
            </li>
            <li>
                You must allow attending Care Workers to complete and perform their
                duties in the time allocated to You.
            </li>
            <li>
                You must make full disclosure to Us of any matter which relates to
                or which may affect Us or any Care Worker in providing care
                services to You. For example (without limitation), if You believe
                that the care services may pose a risk or danger to You, You must
                immediately advise the attending Care Worker of Your concerns. You
                must also treat attending Care Workers with dignity and respect,
                and You must ensure that Your home is safe for attending Care
                Workers.
            </li>
            <li>
                We will ensure that the Care Worker scheduled to provide the
                relevant care services to You will do so with the utmost
                professionalism, due care, skill and diligence. Without limiting
                the foregoing, We will ensure that the care services We provide
    comply with the Home Care Common Standards which are set out in the            <em>Quality of Care Principles 2014</em>.
            </li>
            <li>
                If You have any legitimate concerns about the level of
                professionalism, care, skill and diligence used, We request and
                encourage You to provide Us with feedback in relation to the care
                services You received from the Care Worker, and We will take such
                action as we deem appropriate in relation to the feedback. Where We
                discuss Your feedback with the Care Worker concerned, We will use
                our best endeavours to do so while protecting Your privacy.
            </li>
            <li>
                You still need to pay Your fees and charges to Us in relation to
                the care services We have provided to You (unless those applicable
                fees and charges are paid on Your behalf by Your Approved
                Provider).
            </li>
            <li>
                Where You have scheduled the provision of care services at a
                particular time and at a particular location, it is Your
                responsibility to ensure that You notify Us at least two Business
                Days in advance if You wish to reschedule the care services. You
                must be at Your Home between the designated or agreed times in
                order to receive the scheduled care services. If You are absent, We
                may still charge You for the attendance by the Care Worker.
            </li>
            <li>
                You agree to allow the Care Worker to complete and fulfil their
                duties in the time allocated to You.
            </li>
            <li>
                When booking a scheduled time for the provision of care services,
                You must make full disclosure to Us of any matter which relates or
                which may affect the provision of the care services to You by the
                Care Worker. You also agree to treat the Care Worker with dignity
                and respect, and ensure that Your Home is safe for the Care Worker
                to attend.
            </li>
            <li>
                Without limiting Our obligations owed to You under these Terms of
                Use, We will ensure that the care services provided to You by a
                Care Worker meets the standards required by the Aged Care Act.
            </li>
            <li>
                The care services We agree to provide to You under these Terms of
                Use do not include the provision of aids and equipment to You. You
                agree that You are wholly responsible for the purchase or the hire
                of aids and equipment.
            </li>
            <li>
                If Our Care Worker is present in the event of a medical emergency,
                ambulance support will be sought. If You require urgent care,
                please contact Your general practitioner or dial 000.
            </li>
            <li>
                You agree that when You receive a care service from Us or from a
                Care Worker engaged by Us, You are entitled to provide reasonable
                feedback in relation to the care service. You also agree that You
                are entitled to make reasonable requests in relation to the care
                service to be provided by Us or by a Care Worker engaged by Us, and
                We will use our best endeavours to ensure that your reasonable
                requests are met.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Suspension of the provision of care services:</strong>
        </li>
    </ol>
    <p>
        You acknowledge and agree that We may at any time suspend or cease the
        provision of care services to You if any of the following occurs:
    </p>
    <ol>
        <ol>
            <li>
                You cannot be cared for with the personnel available to Us;
            </li>
            <li>
                You cease to hold a Home Care Package which accords with the
                resources available to Us;
            </li>
            <li>
                You move to an area where We do not provide the services or Your
                needs or condition changes to the extent that You no longer need
                home care or Your needs as assessed by an aged care assessment team
                can be more appropriately met by other types of care;
            </li>
            <li>
                You do not meet Your responsibilities for a reason within Your
                control including (but not limited to) Your responsibilities to pay
                Your fees;
            </li>
            <li>
                access to Your home poses an unacceptable risk to the health,
                safety and/or wellbeing of Care Givers or others; or
            </li>
            <li>
                We give You at least seven days' notice of Our decision to cease
                providing the care services for any other reason.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>User ID, Password and Security: </strong>
            You acknowledge and agree that You are responsible for the security and
            safety of Your unique User ID and Password combination and that You
            must take all necessary steps to protect Your User ID and Password from
            unauthorised access, use or disclosure by any person. Without limiting
            the foregoing, You agree to notify Us immediately when You become aware
            of any unauthorised access to, use or disclosure of Your User ID or
            Password by any person and comply with any directions relating to
            security which We issue or which are issued on Our behalf from time to
            time.
        </li>
        <li>
            <strong></strong>
            <strong>Fees and Charges: </strong>
        </li>
        <ol>
            <li>
                <strong></strong>
                Where we provide You with access to and enjoyment of the App, the
                Portal and the care services (or any part thereof) in consideration
                of Your payment of fees and charges, You agree to promptly pay to
                Us the full amount of the relevant fees and charges in accordance
                with the fee schedule We make available on the Portal or otherwise
                make available to You from time to time.
            </li>
            <li>
                <strong></strong>
                Without limiting the foregoing, where You are required to make
                regular payments of applicable fees and charges, You agree that We
                may enter into a separate direct debit agreement with You to enable
                Us to directly debit the amount to be paid from Your nominated bank
                account.
            </li>
            <li>
                <strong></strong>
                Additionally, You agree and authorised Us to liaise with Your
                Approved Provider (where You are a Client receiving care services
                from Us pursuant to a Home Care Package) in order to obtain payment
                for the care services We provide to You pursuant to Your Care Plan
                and where We receive full payment from Your Approved Provider for
                the care services We provide, We acknowledge and agree that Your
                liability to pay us for the care services provided has been fully
                discharged.
            </li>
            <li>
                <strong></strong>
                Where GST is applicable to some or all of the care services You
                have engaged Us to provide care services to You and You do not have
                a Home Care Package with an Approved Provider, then You promptly
                agree to pay to Us the amount of the consideration we calculate and
                invoice as being due and payable for the provision of care
                services.
            </li>
            <li>
                <strong></strong>
                The consideration payable for any taxable supply We make to You
                under these Terms of Use is expressed as a GST-exclusive amount,
                unless it is specifically referred to as GST-inclusive.
            </li>
            <li>
                <strong></strong>
                In addition to any consideration You are required to pay to Us
                under these Terms of Use, You must also pay to Us an amount equal
                to the GST imposed on each taxable supply (if any) by Us in respect
                of that consideration on the date the consideration is payable
                under these Terms of Use.
            </li>
            <li>
                <strong></strong>
                We acknowledge and agree that You are not obliged to pay an amount
                equal to the GST imposed on any taxable supply made under these
                Terms of Use until You receive from Us a tax invoice or adjustment
                note for the supply to which the payment relates.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>General Use - Permissions and Restrictions: </strong>
            We hereby grant to You permission to access and to use the App, the
            Portal, the tools We make available for use via the App and the Portal
            and to access the Content made available via the Portal (subject to
            these Terms of Use), provided that:
        </li>
        <ol>
            <li>
                You agree not to distribute in any medium any part of the Portal or
                the Content without Our prior written consent, unless We expressly
                and directly make available to You the means for such distribution
                through functionality We offer to you now or in the future;
            </li>
            <li>
                You agree not to alter or modify any part of the Portal or, subject
                to these Terms of Use, the Content made available via the Portal;
            </li>
            <li>
                You agree not to access Content through any technology or means
                other than through the means that We provide or make available to
                You under these Terms of Use;
            </li>
            <li>
                Without limiting the foregoing, You agree not to use or access the
                Portal or the Content for any of the following commercial uses
                unless you first obtain Our prior written consent (which We may
                provide in our absolute discretion, subject to any conditions which
                We choose to impose at our absolute discretion):
            </li>
            <ol>
                <li>
                    the sale of access to the Portal and/or to the Content;
                </li>
                <li>
                    the sale of advertising, sponsorships or promotions placed on
                    or within the Portal or the Content; or
                </li>
                <li>
                    the sale of advertising, sponsorships or promotions on any page
                    of any third party website that may contain Content delivered
                    via the Portal;
                </li>
            </ol>
            <li>
                When You download and install the App on Your smart device, You
                agree that it may automatically download and install updates from
                time to time from Us. These updates are designed to improve,
                enhance and further develop the App and may take the form of bug
                fixes, enhanced functions, new software modules and completely new
                versions. You agree to receive such updates (and to permit Us to
                deliver these to You) as part of Your use of the App, the Portal
                and the associated software We make available to You from time to
                time. You acknowledge and agree that We make no representation or
                warranty to You that We will provide or make available updates or
                new releases of any software associated with the Portal or the
                Services.
            </li>
            <li>
                In Your use of the Portal and Your access to the Content made
                available via the Portal, You agree to comply with all applicable
                laws.
            </li>
            <li>
                We reserve the right to suspend and/or discontinue any aspect of
                the App, the Portal and the associated services at any time. While
                We agree to use all reasonable endeavours to notify You in advance
                of where We decide to discontinue any aspect of the App, the Portal
                and the associated services, to the maximum extent permitted by
                law, We are not liable to You in the event We exercise Our right
                under these Terms of Use to suspend and/or discontinue any aspect
                of the App, the Portal and/or the associated services.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Privacy:</strong>
        </li>
        <ol>
            <li>
                We take all reasonable steps to protect the privacy of Your
                Personal Information in accordance with Our statutory obligations
                under the <em>Privacy Act 1988</em> (Cth).
            </li>
            <li>
                In particular, We comply with Our privacy policy, which You can
                review and access at the following URL: [insert a link to KoolKanya
                privacy policy].
            </li>
            <li>
                You acknowledge and agree, without limiting Our privacy policy or
                these Terms of Use, that:
            </li>
            <ol>
                <li>
                    if You do not agree to provide Personal Information to Us when
                    requested, then You may be unable to access and use part or all
                    of the App or the Portal, or to receive care services from a
                    Care Worker; and
                </li>
                <li>
                    when You provide us with Personal Information on Our request,
                    We may use such Personal Information to create a user profile
                    for You via the Portal. This user profile stores Your
                    individual information (which may include Personal Information
                    about You) and includes settings We generate in relation to
                    Your access privileges to the Portal and the Content on the
                    Portal.
                </li>
            </ol>
            <li>
                Where You are a Client, You agree that We may disclose Personal
                Information about You to Your Approved Provider for the purpose of
                Us completing Our obligations owed to You under these Terms of Use
                (including for the purpose of Us providing care services to You)
                and for Us to receive payment from Your Approved Provider on Your
                behalf.
            </li>
            <li>
                Without limiting the foregoing, where You are a Client, You agree
                that We may disclose Personal Information about You to Your Client
                Advocate, your nominated representatives and contacts and that
                these individuals may speak on your behalf. On request, You agree
                to provide Us with conclusive evidence of the authority Your
                representatives have to deal with or manage Your affairs (or they
                must do so on Your behalf).
            </li>
            <li>
                Where You are a Client, You acknowledge and agree that You have the
                right and capacity (through the tools We provide or make available
                to You via the App and the Portal) to direct whom has access to the
                notes and information We host and hold about the care services
                provided to You.
            </li>
            <li>
                Where You are authorised by a Client to have access to the notes
                and information We host and hold about the care services provided
                to the Client (including nominated and authorised as the Client's
                Client Advocate), You acknowledge and agree that You may only
                access that information and those notes for personal purposes only
                and not for any commercial purposes. Without limiting the
                foregoing, You acknowledge and agree that You may not disclose any
                of the information and notes to any third party without the
                permission of the Client.
            </li>
            <li>
                Generally, We may also from time to time disclose Your Personal
                Information We have collected about You to third parties in
                accordance with Our privacy policy.
            </li>
            <li>
                In accordance with Our privacy policy, We will provide You with
                access to the Personal Information We hold about You.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Indemnity:</strong>
        </li>
    </ol>
    <p>
        To the maximum extent permitted by law, where You are a Client, We are not
        liable to You for and You agree to hold Us harmless, indemnify Us and keep
        Us indemnified from and against any liability, claims, damages or expenses
        of any kind arising directly or indirectly in connection with:
    </p>
    <ol>
        <ol>
            <li>
                any services that are provided to You by a third party;
            </li>
            <li>
                any services provided to You after Your Home Care Package is
                transferred to another provider;
            </li>
            <li>
                the suspension or termination of Your Home Care Package for any
                reason;
            </li>
            <li>
                informal services provided to You by Your family members,
                neighbours or friends; or
            </li>
            <li>
                the disclosure to a third party You have nominated of or the
                granting to a third party You have nominated of access to the notes
                and information We hold about You in connection with the care
                services We provide to You under these Terms of Use where You have
                authorised Us to disclose or grant access to that third party.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Your Content and Conduct:</strong>
        </li>
        <ol>
            <li>
                Regardless of whether You are a Client or not, You are solely
                responsible for Your own Content (including feedback, notes and
                information about the care services provided to You as a Client)
                and the consequences of submitting the Content to Us.
            </li>
            <li>
                You represent and warrant to Us on a continuous basis that You own
                or You have the necessary licences, rights, consents and
                permissions to submit the Content to Us and that Our reproduction,
                communication to the public and other exploitation of the Content
                (including the IPR in the Content) does not infringe the rights
                (including the IPR) of any third party.
            </li>
            <li>
                Subject to the above, You further agree that the Content submitted
                to Us does not contain third party copyrighted material, or
                material that is subject to any proprietary rights (including IPR)
                owned or controlled by any third party, unless You have the
                permission from the lawful owner of the material or that You are
                otherwise legally entitled to submit the Content to Us and to grant
                to Us all of the licence rights set out in these Terms of Use.
            </li>
            <li>
                You additionally agree that You will not submit to Us any Content
                that is:
            </li>
            <ol>
                <li>
                    vulgar or obscene;
                </li>
                <li>
                    discriminatory, sexist or racist;
                </li>
                <li>
                    violent or graphic;
                </li>
                <li>
                    contrary to applicable laws in Australia; or
                </li>
                <li>
                    libellous, tortious, defamatory or invasive of another person's
                    privacy.
                </li>
            </ol>
            <li>
                You acknowledge and agree that We may, at our absolute discretion,
                remove Content which in Our reasonable opinion breaches any of the
                restrictions set out above at clause 11.4. To the maximum extent
                permitted by law, We are not liable to You for any loss or damage
                that You suffer or sustain as a result of Us removing any Content
                in reliance on this clause.
            </li>
            <li>
                You also acknowledge and agree that the fact that We make available
                Your Content to other users of the App and the Portal does not mean
                that We endorse such Content or the person submitting the Content.
            </li>
            <li>
                You further acknowledge and agree that, to the maximum extent
                permitted by law, You indemnify Us, keep Us indemnified and hold Us
                harmless from and against any claims made against Us by any third
                party with respect to the Content (or any part of the Content),
                including the publication or release of the Content, or any
                imputations set out or contained in the Content.
            </li>
            <li>
                Additionally, You further acknowledge and agree that any links to
                external websites controlled or operated by third parties are
                beyond Our reasonable control and that We are not responsible for
                the content accessible via the link(s) included in Your Content. To
                the maximum extent permitted by law, You acknowledge and agree that
                We are not responsible for any loss that You suffer or incur as a
                result of any third party website that is external to the App or
                the Portal being disabled or being unavailable for any reason.
            </li>
            <li>
                We agree that We will use all reasonable endeavours to preserve the
                Content in a readily accessible format (including, where reasonably
                possible, the format in which You uploaded or otherwise submitted
                the Content to the Portal) and to ensure that the Content can be
                accessed at any time.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Communications:</strong>
            Unless You expressly inform Us otherwise (including by sending an email
            to Us unsubscribing from Our email list), You consent to Your receipt,
            from Us, electronic messages (including email) attaching newsletters
            and/or updates of future developments.
        </li>
        <li>
            <strong></strong>
            <strong>Data Transmissions:</strong>
            You acknowledge and agree that the Internet is an inherently insecure
            medium, that no data transmission over the Internet can be guaranteed
            as being totally secure and that Your data (including the Content You
            upload or otherwise submit to Us) is stored on the Portal at Your own
            risk. While We take all reasonable steps to protect and secure Your
            data transmissions to the Portal once We receive the transmission, You
            agree that to the maximum extent permitted by law, We are not liable to
            You for any misuse of or unauthorised access to, use of or disclosure
            of Your data, unless such misuse of or unauthorised access to, use of
            or disclosure of Your data is as a direct result of any negligent,
            reckless or intentional act or omission by Us or by Our employees.
        </li>
        <li>
            <strong></strong>
            <strong>Intellectual Property:</strong>
        </li>
        <ol>
            <li>
                Subject to the remainder of this clause 14 (Intellectual Property),
                You agree as follows:
            </li>
            <ol>
                <li>
                    as between You and Us, all IPR in the App and the Portal
                    (including the Content comprising the App and the Portal) is
                    owned by Us;
                </li>
                <li>
                    to the extent You upload to or provide Content for the App and
                    the Portal, You own the IPR in such Content; and
                </li>
                <li>
                    to the extent that parties other than You and Us upload or
                    provide Content for the Portal, those other parties own the IPR
                    in such Content.
                </li>
            </ol>
            <li>
                We acknowledge and agree that any Content which You upload or
                otherwise provide to Us to be made available on the Portal remains
                Your property, save that You grant to Us (including the right to
                grant sub-licences to other Users who have access to Your Content
                under these Terms of Use) a non-exclusive, perpetual and
                irrevocable, worldwide, fully paid up licence of the IPR in such
                Content for any purpose.
            </li>
            <li>
                Where You have the right under these Terms of Use to access the
                Content of third parties (including the Content of a Client to
                which You are linked by these Terms of Use) then You are granted a
                limited sub-licence by Us on behalf of the third party to access
                and view such Content via the Portal. Such licence does not extend
                to downloading, reproducing, making available, communicating to the
                public or otherwise exploiting the Content (including the IPR in
                the Content).
            </li>
            <li>
                We grant to You a non-exclusive, fully paid up and non-transferable
                licence of the IPR in the Portal and the tools we make available
                via the Portal for Your sole use. You acknowledge and agree that
                the licence We grant to You under this clause is for personal and
                non-commercial use only.
            </li>
            <li>
                Except as permitted by law and otherwise to enable You to make full
                of the Portal in accordance with any operating instructions We make
                available, You acknowledge and agree that You cannot reproduce,
                adapt, modify the Portal, the Content or the tools We make
                available via the Portal otherwise create derivative works
                therefrom.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Warranties and Representations:</strong>
        </li>
        <ol>
            <li>
                We take all reasonable steps to ensure that the Portal and the
                Content can be accessed at any time. However, notwithstanding the
                foregoing, You acknowledge and agree that the Portal and the
                Content are made available to You on an "as is" and "as available"
                basis.
            </li>
            <li>
                You also acknowledge and agree that the Portal and the Content made
                available via the Portal contain or comprise confidential and
                sensitive information (including where supplied by third parties to
                Us and made available to You in accordance with these Terms of
                use). You acknowledge and agree that You will maintain the
                confidentiality of all such information and You agree to
                immediately notify Us if You become aware of any actual or
                suspected unauthorised access to or use or disclosure of such
                information.
            </li>
            <li>
                While We take all reasonable steps to maintain the accuracy of
                information contained on the Portal (including the Content), You
                acknowledge and agree that, to the maximum extent permitted by law,
                We make no warranty or representation, nor give any guarantee as to
                the accuracy, adequacy or completeness of the Content.
            </li>
            <li>
                To the maximum extent permitted by law, We make no representation
                or warranty, nor give any guarantee to You in relation to the
                Portal or to the Content, including (without limitation):
            </li>
            <ol>
                <li>
                    as to the reliability, suitability or availability of the
                    Portal or the Content;
                </li>
                <li>
                    that access to the Portal and/or to the Content will be secure,
                    uninterrupted or error-free;
                </li>
                <li>
                    that errors or defects with the Portal and/or the Content will
                    be able to be corrected;
                </li>
                <li>
                    that the Portal will operate with any other hardware, software,
                    system or data; or
                </li>
                <li>
                    the accuracy and fairness of the Content.
                </li>
            </ol>
        </ol>
        <li>
            <strong></strong>
            <strong>Liability:</strong>
        </li>
        <ol>
            <li>
                The liability of either party for a breach of these Terms of Use or
                for any other common law or statutory cause of action arising out
                of the operation of these Terms of Use will be determined under the
                relevant law that is recognised, and would be applied by, the High
                Court of Australia.
            </li>
            <li>
                To the fullest extent permitted by law, in no event shall We, Our
                directors, officers, employees or agents, be liable to You for any
                direct, indirect, incidental, special or consequential loss or
                damage arising from any:
            </li>
            <ol>
                <li>
                    errors, mistakes or inaccuracies of Content;
                </li>
                <li>
                    personal injury or property damage (of any nature whatsoever)
                    arising or resulting from Your access to and use of the App,
                    the Portal and/or the Content;
                </li>
                <li>
                    any unauthorised access to or use of Our storage system that
                    hosts Your Content, except where such unauthorised access to or
                    use of Our storage system is as a result of some negligent,
                    reckless or wilful act or omission by Us;
                </li>
                <li>
                    any interruption or cessation of transmission to or from the
                    App or the Portal;
                </li>
                <li>
                    any bugs, viruses and/or other malicious code which may be
                    transmitted to or through the App or the Portal by any third
                    party (except where such transmission is facilitated by some
                    negligent, reckless or wilful act or omission by Us); and/or
                </li>
                <li>
                    any errors or omissions in any Content or for any loss or
                    damage of any kind incurred as a result of Your use of any
                    Content posted, emailed, transmitted or otherwise made
                    available via the Portal, whether based on warranty, contract,
                    tort or any other legal theory.
                </li>
            </ol>
            <li>
                To the extent that We cannot lawfully exclude Our warranty to You
                under these Terms of Use, then to the extent permitted by law, We
                limit Our liability to You to the cost of resupplying the products
                and/or services to You as contemplated under these Terms of Use.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Notices:</strong>
            Without limiting the methods by which a notice or other communication
            may be given at law, a notice or other communication is properly given
            or served if it is transmitted by electronic mail or other electronic
            means to the electronic mail address associated with Your User ID. You
            agree to promptly notify Us of any change in the electronic mail
            address associated with Your User ID.
        </li>
        <li>
            <strong></strong>
            <strong>Entire Agreement:</strong>
            These Terms of Use constitute the entire agreement with respect to Your
            use of the Portal and Your right to upload, submit, access and use the
            Content. These Terms of Use supersede all prior representations,
            agreements, statements and understandings (whether verbal or in
            writing) in relation to the subject matter of these Terms of Use.
        </li>
        <li>
            <strong></strong>
            <strong>Applicable Law:</strong>
            These Terms of Use are subject to the laws in force in the State of
            Victoria and You and We both agree to submit to the exclusive
            jurisdiction of the courts of the State of Victoria with respect to any
            disputes arising out of or in connection with these Terms of Use.
        </li>
        <li>
            <strong></strong>
            <strong>Variation:</strong>
            We may vary these Terms of Use at any time by giving You notice of the
            variation. Such notice may be given (and You are deemed to have been
            given notice) by posting a notice of the variation to these Terms of
            Use reasonably prominently on the Portal. We will use all reasonable
            endeavours to notify You of any variations to these Terms of Use at
            least 14 days in advance of the variation taking effect. Such
            notification will take the form of an email sent to the email address
            You provide to Us (as updated or replaced from time to time by You), by
            posting a reasonably prominent link to the varied terms on Our website
            and/or by posting a reasonably prominent notice in any app We release
            to support the Portal.
        </li>
        <li>
            <strong></strong>
            <strong>Waiver: </strong>
            A waiver by either party of a breach of these Terms of Use will not be
            regarded as a waiver of any other breach. A party's failure to enforce
            a provision of these Terms of Use will not be interpreted as a waiver.
        </li>
        <li>
            <strong></strong>
            <strong>Severability:</strong>
            Each provision of these Terms of Use shall be read as separate and
            separable so that if any provision is void or unenforceable for any
            reason, that provision will be severed and the remainder will be
            construed as if the severed provision had never existed.
        </li>
        <li>
            <strong></strong>
            <strong>Assignment: </strong>
            We may assign any rights under these Terms of Use to a third party
            without Your prior written consent. You acknowledge and agree that the
            rights conferred on You under these Terms of Use are personal to You
            and cannot be assigned or novated to another party except with Our
            prior written consent.
        </li>
        <li>
            <strong></strong>
            <strong>Further acts: </strong>
            Each party must, without further consideration, sign, execute and
            deliver any document and perform any other act that is necessary or
            desirable to give full effect to these Terms of Use.
        </li>
        <li>
            <strong></strong>
            <strong>Party preparing document not to be disadvantaged: </strong>
            No rule of contract interpretation must be applied in the
            interpretation of these Terms of Use to the disadvantage of one party
            on the basis that it prepared or put forward these Terms of Use or any
            document comprising these Terms of Use.
        </li>
        <li>
            <strong></strong>
            <strong>Termination and Survival:</strong>
        </li>
        <ol>
            <li>
                These Terms of Use (except for those clauses which are expressly
                stated as surviving the termination or expiry of these Terms of
                Use) expire on the earliest of the following:
            </li>
            <ol>
                <li>
                    Your death;
                </li>
                <li>
                    the cessation of Our provision of the App, the Portal and the
                    related tools and services;
                </li>
                <li>
                    Your voluntary decision to cease procuring care services from
                    Us by notice to Us through the tools We make available via the
                    App or the Portal; and
                </li>
                <li>
                    Our termination of Your access rights to the Portal (for any of
                    the reasons contemplated in these Terms of Use).
                </li>
            </ol>
            <li>
                Clauses 9 (Privacy), 10 (Indemnity), 12 (Communications), 13 (Data
                Transmission), 14 (Intellectual Property), 15 (Warranties and
                Representations) and 16 (Liability) survive the termination or
                expiry of these Terms of Use.
            </li>
        </ol>
        <li>
            <strong></strong>
            <strong>Interpretation: </strong>
            In these Terms of Use, unless the contrary intention appears:
        </li>
        <ol>
            <li>
                the singular includes the plural and vice versa;
            </li>
            <li>
                words importing one gender include other genders;
            </li>
            <li>
                a reference to a document or instrument (including these Terms of
                use) includes that document or instrument as novated, altered or
                replaced from time to time;
            </li>
            <li>
                a reference to an individual or person includes a partnership, body
                corporate, government authority or agency and vice versa;
            </li>
            <li>
                a reference to a party includes that party's executors,
                administrators, successors, substitutes and permitted assigns;
            </li>
            <li>
                other grammatical forms of defined words or expressions have
                corresponding meanings;
            </li>
            <li>
                a covenant, undertaking, representation, warranty, indemnity or
                agreement made or given by two or more parties or a party comprised
                of two or more persons is made or given and binds those parties or
                persons jointly and severally;
            </li>
            <li>
                a reference to a statute, code or other law includes regulations
                and other instruments made under it and includes consolidations,
                amendments, reenactments or replacements of any of them;
            </li>
            <li>
                all monetary amounts are in Australian dollars;
            </li>
            <li>
                a party that is a trustee is bound both personally and in its
                capacity as trustee; and
            </li>
            <li>
    a reference to an authority, institution, association or body ("            <strong>original entity</strong>") that ceased to exist, been
                reconstituted, renamed or replaced or whose powers or functions
                have been transferred to another entity, is a reference to the
                entity that most closely serves the purposes or objects of the
                original entity.
            </li>
        </ol>
    </ol>
</html>`


export const JqueryHtml = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
`;

//     `<html>
// <head>
//     <title>My Sample</title>
//     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
//     <script type="text/javascript">
//         var new_num = 40; // global variable
//         jQuery(document).ready(function(){
//         http://api.jquery.com/css/
//         $('p').css({border: '5px solid red', color: 'green', padding: '20px'});
//
//         // http://api.jquery.com/bind/
//         // http://api.jquery.com/css/
//         // http://api.jquery.com/text/
//         $('#countme').css({
//         margin: '0 auto',
//         backgroundColor: '#cc6a3a',
//         textAlign: 'center',
//         fontSize: '40px'
//     }).bind('click mousemove', function(){
//         var current = parseInt($(this).text(), 10);
//         new_num = new_num + 1
//         $(this).text(current+1).css({fontSize: new_num+'px'})
//     });
//     });
//     </script>
// </head>
// <body>
//
// <p>Hello</p>
//
// <p>World</p>
//
// <div id="countme">0</div>
//
// <a href="http://api.jquery.com/">Learn more about jQuery's methods</a>
//
// </body>
// </html>`







let KKHtml= `<html><head>
    <title>The Comprehensive Guide to Maternity Leave &amp; Benefits in India</title>
    <meta charSet="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="dns-prefetch" href="//fonts.googleapis.com">
                <link rel="dns-prefetch" href="//s.w.org">
                    <link rel="alternate" type="application/rss+xml" title="KoolKanya » Feed"
                          href="https://koolkanya.com/blogs/feed/">
                        <link rel="alternate" type="application/rss+xml" title="KoolKanya » Comments Feed"
                              href="https://koolkanya.com/blogs/comments/feed/">
                            <link rel="alternate" type="application/rss+xml"
                                  title="KoolKanya » Everything You Need To Know About Maternity Leave &amp; Benefits In India Comments Feed"
                                  href="https://koolkanya.com/blogs/restarting/motherhood-at-work/maternity-leave-benefits-india-planning-guide/feed/">
                                <script type="text/javascript">
                                    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/koolkanya.com\/blogs\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.2"}};
                                    !function (a, b, c){function d(a, b) {
                                    var c = String.fromCharCode;
                                    l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                                    var d = k.toDataURL();
                                    l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                                    var e = k.toDataURL();
                                    return d === e
                                }function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window, document, window._wpemojiSettings);
                                </script>
                                <style type="text/css">
                                    img.wp-smiley,
                                    img.emoji {
                                    display: inline !important;
                                    border: none !important;
                                    box-shadow: none !important;
                                    height: 1em !important;
                                    width: 1em !important;
                                    margin: 0 .07em !important;
                                    vertical-align: -0.1em !important;
                                    background: none !important;
                                    padding: 0 !important;
                                }
                                </style>
                                <link rel="stylesheet" id="dashicons-css"
                                      href="https://koolkanya.com/blogs/wp-includes/css/dashicons.min.css?ver=5.2.2"
                                      type="text/css" media="all">
                                    <link rel="stylesheet" id="admin-bar-css"
                                          href="https://koolkanya.com/blogs/wp-includes/css/admin-bar.min.css?ver=5.2.2"
                                          type="text/css" media="all">
                                        <link rel="stylesheet" id="wp-block-library-css"
                                              href="https://koolkanya.com/blogs/wp-content/plugins/gutenberg/build/block-library/style.css?ver=1566285648"
                                              type="text/css" media="all">
                                            <link rel="stylesheet" id="contact-form-7-css"
                                                  href="https://koolkanya.com/blogs/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4"
                                                  type="text/css" media="all">
                                                <link rel="stylesheet" id="wordfenceAJAXcss-css"
                                                      href="https://koolkanya.com/blogs/wp-content/plugins/wordfence/css/wordfenceBox.1566486436.css?ver=7.4.0"
                                                      type="text/css" media="all">
                                                    <link rel="stylesheet" id="wordpress-popular-posts-css-css"
                                                          href="https://koolkanya.com/blogs/wp-content/plugins/wordpress-popular-posts/public/css/wpp.css?ver=4.2.2"
                                                          type="text/css" media="all">
                                                        <link rel="stylesheet" id="wtr-css-css"
                                                              href="https://koolkanya.com/blogs/wp-content/plugins/worth-the-read/css/wtr.css?ver=5.2.2"
                                                              type="text/css" media="all">
                                                            <link rel="stylesheet" id="td-plugin-multi-purpose-css"
                                                                  href="https://koolkanya.com/blogs/wp-content/plugins/td-composer/td-multi-purpose/style.css?ver=61179afdbbd6a8d8c8a7f82ae3fcd87d"
                                                                  type="text/css" media="all">
                                                                <link rel="stylesheet" id="td_live_css_frontend-css"
                                                                      href="https://koolkanya.com/blogs/wp-content/plugins/td-composer/css-live/assets/css/td_live_css_frontend.css?ver=61179afdbbd6a8d8c8a7f82ae3fcd87d"
                                                                      type="text/css" media="all">
                                                                    <link crossOrigin="anonymous" rel="stylesheet"
                                                                          id="google-fonts-style-css"
                                                                          href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400%2C400italic%2C600%2C600italic%2C700%7CRoboto%3A300%2C400%2C400italic%2C500%2C500italic%2C700%2C900&amp;ver=9.7.3"
                                                                          type="text/css" media="all">
                                                                        <link rel="stylesheet"
                                                                              id="yoast-seo-adminbar-css"
                                                                              href="https://koolkanya.com/blogs/wp-content/plugins/wordpress-seo-premium/css/dist/adminbar-1190.min.css?ver=11.9"
                                                                              type="text/css" media="all">
                                                                            <link rel="stylesheet" id="td-theme-css"
                                                                                  href="https://koolkanya.com/blogs/wp-content/themes/Newspaper/style.css?ver=9.7.3"
                                                                                  type="text/css" media="all">
                                                                                <style id="td-theme-inline-css"
                                                                                       type="text/css">

                                                                                    @media (max-width: 767px) {
                                                                                    .td-header-desktop-wrap {
                                                                                    display: none;
                                                                                }
                                                                                }
                                                                                    @media (min-width: 767px) {
                                                                                    .td-header-mobile-wrap {
                                                                                    display: none;
                                                                                }
                                                                                }
                                                                                </style>
                                                                                <link rel="stylesheet"
                                                                                      id="js_composer_front-css"
                                                                                      href="https://koolkanya.com/blogs/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.5.1"
                                                                                      type="text/css" media="all">
                                                                                    <link rel="stylesheet"
                                                                                          id="td-legacy-framework-front-style-css"
                                                                                          href="https://koolkanya.com/blogs/wp-content/plugins/td-composer/legacy/Newspaper/assets/css/td_legacy_main.css?ver=61179afdbbd6a8d8c8a7f82ae3fcd87d"
                                                                                          type="text/css" media="all">
                                                                                        <link rel="stylesheet"
                                                                                              id="td-theme-demo-style-css"
                                                                                              href="https://koolkanya.com/blogs/wp-content/plugins/td-composer/legacy/Newspaper/includes/demos/blog_coffee/demo_style.css?ver=9.7.3"
                                                                                              type="text/css"
                                                                                              media="all">
                                                                                            <link rel="stylesheet"
                                                                                                  id="tdb_front_style-css"
                                                                                                  href="https://koolkanya.com/blogs/wp-content/plugins/td-cloud-library/assets/css/tdb_less_front.css?ver=d158fac1e2f85794ec26781eb2a38fd9"
                                                                                                  type="text/css"
                                                                                                  media="all">
                                                                                                <script
                                                                                                    type="text/javascript"
                                                                                                    src="https://koolkanya.com/blogs/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp"></script>
                                                                                                <script
                                                                                                    type="text/javascript"
                                                                                                    src="https://koolkanya.com/blogs/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
                                                                                                <script
                                                                                                    type="text/javascript">
                                                                                                    /* <![CDATA[ */
                                                                                                    var WFAJAXWatcherVars = {"nonce":"511f24b412"};
                                                                                                    /* ]]> */
                                                                                                </script>
                                                                                                <script
                                                                                                    type="text/javascript"
                                                                                                    src="https://koolkanya.com/blogs/wp-content/plugins/wordfence/js/admin.ajaxWatcher.1566486436.js?ver=7.4.0"></script>
                                                                                                <script
                                                                                                    type="text/javascript">
                                                                                                    /* <![CDATA[ */
                                                                                                    var wpp_params = {"sampling_active":"0","sampling_rate":"100","ajax_url":"https:\/\/koolkanya.com\/blogs\/wp-json\/wordpress-popular-posts\/v1\/popular-posts\/","ID":"1335","token":"6c36799c6a","debug":""};
                                                                                                    /* ]]> */
                                                                                                </script>
                                                                                                <script
                                                                                                    type="text/javascript"
                                                                                                    src="https://koolkanya.com/blogs/wp-content/plugins/wordpress-popular-posts/public/js/wpp-4.2.0.min.js?ver=4.2.2"></script>
                                                                                                <link
                                                                                                    rel="https://api.w.org/"
                                                                                                    href="https://koolkanya.com/blogs/wp-json/">
                                                                                                    <link rel="EditURI"
                                                                                                          type="application/rsd+xml"
                                                                                                          title="RSD"
                                                                                                          href="https://koolkanya.com/blogs/xmlrpc.php?rsd">
                                                                                                        <link
                                                                                                            rel="wlwmanifest"
                                                                                                            type="application/wlwmanifest+xml"
                                                                                                            href="https://koolkanya.com/blogs/wp-includes/wlwmanifest.xml">


                                                                                                            <link
                                                                                                                rel="alternate"
                                                                                                                type="application/json+oembed"
                                                                                                                href="https://koolkanya.com/blogs/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fkoolkanya.com%2Fblogs%2Frestarting%2Fmotherhood-at-work%2Fmaternity-leave-benefits-india-planning-guide%2F">
                                                                                                                <link
                                                                                                                    rel="alternate"
                                                                                                                    type="text/xml+oembed"
                                                                                                                    href="https://koolkanya.com/blogs/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fkoolkanya.com%2Fblogs%2Frestarting%2Fmotherhood-at-work%2Fmaternity-leave-benefits-india-planning-guide%2F&amp;format=xml">
                                                                                                                    <style
                                                                                                                        type="text/css">.wtr-time-wrap{
                                                                                                                        /* wraps the entire label */
                                                                                                                        margin: 0 10px;

                                                                                                                    }
                                                                                                                        .wtr-time-number{
                                                                                                                            /* applies only to the number */

                                                                                                                        }</style>
                                                                                                                    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
                                                                                                                    <script
                                                                                                                        type="text/javascript">
                                                                                                                        window.tdb_globals = {"wpRestNonce":"6c36799c6a","wpRestUrl":"https:\/\/koolkanya.com\/blogs\/wp-json\/","permalinkStructure":"\/%category%\/%postname%\/","isAjax":false,"isAdminBarShowing":true,"autoloadScrollPercent":50,"origPostEditUrl":"https:\/\/koolkanya.com\/blogs\/wp-admin\/post.php?post=1335&amp;action=edit"};
                                                                                                                    </script>
                                                                                                                    <script
                                                                                                                        type="text/javascript">
                                                                                                                        window.tdwGlobal = {"adminUrl":"https:\/\/koolkanya.com\/blogs\/wp-admin\/","wpRestNonce":"6c36799c6a","wpRestUrl":"https:\/\/koolkanya.com\/blogs\/wp-json\/","permalinkStructure":"\/%category%\/%postname%\/"};
                                                                                                                    </script>

                                                                                                                    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://koolkanya.com/blogs/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
                                                                                                                    <link
                                                                                                                        rel="amphtml"
                                                                                                                        href="https://koolkanya.com/blogs/restarting/motherhood-at-work/maternity-leave-benefits-india-planning-guide/?amp">
                                                                                                                        <style
                                                                                                                            type="text/css"
                                                                                                                            media="print">#wpadminbar {display:none;}</style>
                                                                                                                        <style
                                                                                                                            type="text/css"
                                                                                                                            media="screen">
                                                                                                                            html {margin - top: 32px !important;}
                                                                                                                            *
                                                                                                                            html
                                                                                                                            body {margin - top: 32px !important;}
                                                                                                                            @media
                                                                                                                            screen
                                                                                                                            and
                                                                                                                            (
                                                                                                                            max-width:
                                                                                                                            782px
                                                                                                                            ) {
                                                                                                                            html {margin-top: 46px !important;}
                                                                                                                            * html body {margin-top: 46px !important;}
                                                                                                                        }
                                                                                                                        </style>

                                                                                                                        <!-- JS generated by theme -->

                                                                                                                        <script
                                                                                                                            type="text/javascript">
                                                                                                                            var tdBlocksArray = []; //here we store all the items for the current page
                                                                                                                            //td_block class - each ajax block uses a object of this class for requests
                                                                                                                            function tdBlock() {
                                                                                                                            this.id = '';
                                                                                                                            this.block_type = 1; //block type id (1-234 etc)
                                                                                                                            this.atts = '';
                                                                                                                            this.td_column_number = '';
                                                                                                                            this.td_current_page = 1; //
                                                                                                                            this.post_count = 0; //from wp
                                                                                                                            this.found_posts = 0; //from wp
                                                                                                                            this.max_num_pages = 0; //from wp
                                                                                                                            this.td_filter_value = ''; //current live filter value
                                                                                                                            this.is_ajax_running = false;
                                                                                                                            this.td_user_action = ''; // load more or infinite loader (used by the animation)
                                                                                                                            this.header_color = '';
                                                                                                                            this.ajax_pagination_infinite_stop = ''; //show load more at page x
                                                                                                                        }
                                                                                                                            // td_js_generator - mini detector
                                                                                                                            (function (){
                                                                                                                            var htmlTag = document.getElementsByTagName("html")[0];

                                                                                                                            if ( navigator.userAgent.indexOf("MSIE 10.0") > -1 ) {
                                                                                                                            htmlTag.className += ' ie10';
                                                                                                                        }

                                                                                                                            if ( !!navigator.userAgent.match(/Trident.*rv\:11\./) ) {
                                                                                                                            htmlTag.className += ' ie11';
                                                                                                                        }

                                                                                                                            if ( navigator.userAgent.indexOf("Edge") > -1 ) {
                                                                                                                            htmlTag.className += ' ieEdge';
                                                                                                                        }

                                                                                                                            if ( /(iPad|iPhone|iPod)/g.test(navigator.userAgent) ) {
                                                                                                                            htmlTag.className += ' td-md-is-ios';
                                                                                                                        }

                                                                                                                            var user_agent = navigator.userAgent.toLowerCase();
                                                                                                                            if ( user_agent.indexOf("android") > -1 ) {
                                                                                                                            htmlTag.className += ' td-md-is-android';
                                                                                                                        }

                                                                                                                            if ( -1 !== navigator.userAgent.indexOf('Mac OS X')  ) {
                                                                                                                            htmlTag.className += ' td-md-is-os-x';
                                                                                                                        }

                                                                                                                            if ( /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) ) {
                                                                                                                            htmlTag.className += ' td-md-is-chrome';
                                                                                                                        }

                                                                                                                            if ( -1 !== navigator.userAgent.indexOf('Firefox') ) {
                                                                                                                            htmlTag.className += ' td-md-is-firefox';
                                                                                                                        }

                                                                                                                            if ( -1 !== navigator.userAgent.indexOf('Safari') && -1 === navigator.userAgent.indexOf('Chrome') ) {
                                                                                                                            htmlTag.className += ' td-md-is-safari';
                                                                                                                        }

                                                                                                                            if( -1 !== navigator.userAgent.indexOf('IEMobile') ){
                                                                                                                            htmlTag.className += ' td-md-is-iemobile';
                                                                                                                        }

                                                                                                                        })
                                                                                                                        ();
                                                                                                                            var tdLocalCache = {};
                                                                                                                            ( function () {
                                                                                                                            "use strict";

                                                                                                                            tdLocalCache = {
                                                                                                                            data: {},
                                                                                                                            remove: function (resource_id) {
                                                                                                                            delete tdLocalCache.data[resource_id];
                                                                                                                        },
                                                                                                                            exist: function (resource_id) {
                                                                                                                            return tdLocalCache.data.hasOwnProperty(resource_id) && tdLocalCache.data[resource_id] !== null;
                                                                                                                        },
                                                                                                                            get: function (resource_id) {
                                                                                                                            return tdLocalCache.data[resource_id];
                                                                                                                        },
                                                                                                                            set: function (resource_id, cachedData) {
                                                                                                                            tdLocalCache.remove(resource_id);
                                                                                                                            tdLocalCache.data[resource_id] = cachedData;
                                                                                                                        }
                                                                                                                        };
                                                                                                                        })
                                                                                                                        ();
                                                                                                                            var td_viewport_interval_list = [{"limitBottom":767,"sidebarWidth":228},{"limitBottom":1018,"sidebarWidth":300},{"limitBottom":1140,"sidebarWidth":324}]
                                                                                                                        ;
                                                                                                                            var tdc_is_installed = "yes";
                                                                                                                            var td_ajax_url = "https:\/\/koolkanya.com\/blogs\/wp-admin\/admin-ajax.php?td_theme_name=Newspaper&v=9.7.3";
                                                                                                                            var td_get_template_directory_uri = "https:\/\/koolkanya.com\/blogs\/wp-content\/plugins\/td-composer\/legacy\/common";
                                                                                                                            var tds_snap_menu = "snap";
                                                                                                                            var tds_logo_on_sticky = "show_header_logo";
                                                                                                                            var tds_header_style = "tdm_header_style_3";
                                                                                                                            var td_please_wait = "Please wait
                                                                                                                        ...
                                                                                                                        ";
                                                                                                                            var td_email_user_pass_incorrect = "User or password incorrect
                                                                                                                        !";
                                                                                                                            var td_email_user_incorrect = "Email or username incorrect
                                                                                                                        !";
                                                                                                                            var td_email_incorrect = "Email incorrect
                                                                                                                        !";
                                                                                                                            var tds_more_articles_on_post_enable = "show";
                                                                                                                            var tds_more_articles_on_post_time_to_wait = "";
                                                                                                                            var tds_more_articles_on_post_pages_distance_from_top = 0;
                                                                                                                            var tds_theme_color_site_wide = "#703db2";
                                                                                                                            var tds_smart_sidebar = "enabled";
                                                                                                                            var tdThemeName = "Newspaper";
                                                                                                                            var td_magnific_popup_translation_tPrev = "Previous (Left arrow key
                                                                                                                        )
                                                                                                                        ";
                                                                                                                            var td_magnific_popup_translation_tNext = "Next (Right arrow key
                                                                                                                        )
                                                                                                                        ";
                                                                                                                            var td_magnific_popup_translation_tCounter = "%curr% of %
                                                                                                                        total % ";
                                                                                                                            var td_magnific_popup_translation_ajax_tError = "The content from %
                                                                                                                        url % could not be loaded.
                                                                                                                        ";
                                                                                                                            var td_magnific_popup_translation_image_tError = "The image # % curr % could not be loaded.
                                                                                                                        ";
                                                                                                                            var tdBlockNonce = "1c0c5c9e90";
                                                                                                                            var tdDateNamesI18n ={"month_names":["January","February","March","April","May","June","July","August","September","October","November","December"],"month_names_short":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"day_names":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"day_names_short":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]};
                                                                                                                            var td_ad_background_click_link = "";
                                                                                                                            var td_ad_background_click_target = "";
                                                                                                                        </script>
                                                                                                                        <!-- Header style compiled by theme -->

                                                                                                                        <style>
                                                                                                                            @font-face {
                                                                                                                            font - family: "FFTisaOT";
                                                                                                                            src: local("FFTisaOT"), url("https://koolkanya.com/blogs/wp-content/uploads/2019/05/FFTisaOT.woff") format("woff");
                                                                                                                        }

                                                                                                                            @font-face {
                                                                                                                            font - family: "SourceSansPro-Regular";
                                                                                                                            src: local("SourceSansPro-Regular"), url("https://koolkanya.com/blogs/wp-content/uploads/2019/05/SourceSansPro-Regular.woff") format("woff");
                                                                                                                        }

                                                                                                                            @font-face {
                                                                                                                            font - family: "Gotham-Black";
                                                                                                                            src: local("Gotham-Black"), url("https://koolkanya.com/blogs/wp-content/uploads/2018/09/Gotham-Black.woff") format("woff");
                                                                                                                        }

                                                                                                                            .td-header-wrap
                                                                                                                            .black-menu
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-header-wrap
                                                                                                                            .black-menu
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-header-wrap
                                                                                                                            .black-menu
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-header-wrap
                                                                                                                            .black-menu
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .td-header-wrap
                                                                                                                            .black-menu
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-header-style-12
                                                                                                                            .td-header-menu-wrap-full,
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a:after,
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a:after,
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a:after,
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li:hover
                                                                                                                            >
                                                                                                                            a:after,
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a:after,
                                                                                                                            .td-header-style-12
                                                                                                                            .td-affix,
                                                                                                                            .header-search-wrap
                                                                                                                            .td-drop-down-search:after,
                                                                                                                            .header-search-wrap
                                                                                                                            .td-drop-down-search
                                                                                                                            .btn:hover,
                                                                                                                            input[type=submit]:hover,
                                                                                                                            .td-read-more
                                                                                                                            a,
                                                                                                                            .td-post-category:hover,
                                                                                                                            .td-grid-style-1.td-hover-1
                                                                                                                            .td-big-grid-post:hover
                                                                                                                            .td-post-category,
                                                                                                                            .td-grid-style-5.td-hover-1
                                                                                                                            .td-big-grid-post:hover
                                                                                                                            .td-post-category,
                                                                                                                            .td_top_authors
                                                                                                                            .td-active
                                                                                                                            .td-author-post-count,
                                                                                                                            .td_top_authors
                                                                                                                            .td-active
                                                                                                                            .td-author-comments-count,
                                                                                                                            .td_top_authors
                                                                                                                            .td_mod_wrap:hover
                                                                                                                            .td-author-post-count,
                                                                                                                            .td_top_authors
                                                                                                                            .td_mod_wrap:hover
                                                                                                                            .td-author-comments-count,
                                                                                                                            .td-404-sub-sub-title
                                                                                                                            a:hover,
                                                                                                                            .td-search-form-widget
                                                                                                                            .wpb_button:hover,
                                                                                                                            .td-rating-bar-wrap
                                                                                                                            div,
                                                                                                                            .td_category_template_3
                                                                                                                            .td-current-sub-category,
                                                                                                                            .dropcap,
                                                                                                                            .td_wrapper_video_playlist
                                                                                                                            .td_video_controls_playlist_wrapper,
                                                                                                                            .wpb_default,
                                                                                                                            .wpb_default:hover,
                                                                                                                            .td-left-smart-list:hover,
                                                                                                                            .td-right-smart-list:hover,
                                                                                                                            .woocommerce-checkout
                                                                                                                            .woocommerce
                                                                                                                            input.button:hover,
                                                                                                                            .woocommerce-page
                                                                                                                            .woocommerce
                                                                                                                            a.button:hover,
                                                                                                                            .woocommerce-account
                                                                                                                            div.woocommerce
                                                                                                                            .button:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            button:hover,
                                                                                                                            .bbp_widget_login
                                                                                                                            .button:hover,
                                                                                                                            .td-footer-wrapper
                                                                                                                            .td-post-category,
                                                                                                                            .td-footer-wrapper
                                                                                                                            .widget_product_search
                                                                                                                            input[type="submit"]:hover,
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            a.button:hover,
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            #respond
                                                                                                                            input#submit:hover,
                                                                                                                            .woocommerce
                                                                                                                            .checkout
                                                                                                                            input#place_order:hover,
                                                                                                                            .woocommerce
                                                                                                                            .woocommerce.widget
                                                                                                                            .button:hover,
                                                                                                                            .single-product
                                                                                                                            .product
                                                                                                                            .summary
                                                                                                                            .cart
                                                                                                                            .button:hover,
                                                                                                                            .woocommerce-cart
                                                                                                                            .woocommerce
                                                                                                                            table.cart
                                                                                                                            .button:hover,
                                                                                                                            .woocommerce-cart
                                                                                                                            .woocommerce
                                                                                                                            .shipping-calculator-form
                                                                                                                            .button:hover,
                                                                                                                            .td-next-prev-wrap
                                                                                                                            a:hover,
                                                                                                                            .td-load-more-wrap
                                                                                                                            a:hover,
                                                                                                                            .td-post-small-box
                                                                                                                            a:hover,
                                                                                                                            .page-nav
                                                                                                                            .current,
                                                                                                                            .page-nav:first-child
                                                                                                                            >
                                                                                                                            div,
                                                                                                                            .td_category_template_8
                                                                                                                            .td-category-header
                                                                                                                            .td-category
                                                                                                                            a.td-current-sub-category,
                                                                                                                            .td_category_template_4
                                                                                                                            .td-category-siblings
                                                                                                                            .td-category
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-pagination
                                                                                                                            .current,
                                                                                                                            #bbpress-forums
                                                                                                                            #bbp-single-user-details
                                                                                                                            #bbp-user-navigation
                                                                                                                            li.current
                                                                                                                            a,
                                                                                                                            .td-theme-slider:hover
                                                                                                                            .slide-meta-cat
                                                                                                                            a,
                                                                                                                            a.vc_btn-black:hover,
                                                                                                                            .td-trending-now-wrapper:hover
                                                                                                                            .td-trending-now-title,
                                                                                                                            .td-scroll-up,
                                                                                                                            .td-smart-list-button:hover,
                                                                                                                            .td-weather-information:before,
                                                                                                                            .td-weather-week:before,
                                                                                                                            .td_block_exchange
                                                                                                                            .td-exchange-header:before,
                                                                                                                            .td_block_big_grid_9.td-grid-style-1
                                                                                                                            .td-post-category,
                                                                                                                            .td_block_big_grid_9.td-grid-style-5
                                                                                                                            .td-post-category,
                                                                                                                            .td-grid-style-6.td-hover-1
                                                                                                                            .td-module-thumb:after,
                                                                                                                            .td-pulldown-syle-2
                                                                                                                            .td-subcat-dropdown
                                                                                                                            ul:after,
                                                                                                                            .td_block_template_9
                                                                                                                            .td-block-title:after,
                                                                                                                            .td_block_template_15
                                                                                                                            .td-block-title:before,
                                                                                                                            div.wpforms-container
                                                                                                                            .wpforms-form
                                                                                                                            div.wpforms-submit-container
                                                                                                                            button[type=submit] {
                                                                                                                            background - color: #703db2;
                                                                                                                        }

                                                                                                                            .td_block_template_4
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item:before {
                                                                                                                            border - color: #703db2 transparent transparent transparent !important;
                                                                                                                        }

                                                                                                                            .woocommerce
                                                                                                                            .woocommerce-message
                                                                                                                            .button:hover,
                                                                                                                            .woocommerce
                                                                                                                            .woocommerce-error
                                                                                                                            .button:hover,
                                                                                                                            .woocommerce
                                                                                                                            .woocommerce-info
                                                                                                                            .button:hover {
                                                                                                                            background - color: #703db2 !important;
                                                                                                                        }


                                                                                                                            .td_block_template_4
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_3
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_9
                                                                                                                            .td-related-title:after {
                                                                                                                            background - color: #703db2;
                                                                                                                        }

                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            .onsale,
                                                                                                                            .woocommerce.widget
                                                                                                                            .ui-slider
                                                                                                                            .ui-slider-handle {
                                                                                                                            background: none #703db2;
                                                                                                                        }

                                                                                                                            .woocommerce.widget.widget_layered_nav_filters
                                                                                                                            ul
                                                                                                                            li
                                                                                                                            a {
                                                                                                                            background: none repeat scroll 0 0 #703db2 !important;
                                                                                                                        }

                                                                                                                            a,
                                                                                                                            cite
                                                                                                                            a:hover,
                                                                                                                            .td_mega_menu_sub_cats
                                                                                                                            .cur-sub-cat,
                                                                                                                            .td-mega-span
                                                                                                                            h3
                                                                                                                            a:hover,
                                                                                                                            .td_mod_mega_menu:hover
                                                                                                                            .entry-title
                                                                                                                            a,
                                                                                                                            .header-search-wrap
                                                                                                                            .result-msg
                                                                                                                            a:hover,
                                                                                                                            .td-header-top-menu
                                                                                                                            .td-drop-down-search
                                                                                                                            .td_module_wrap:hover
                                                                                                                            .entry-title
                                                                                                                            a,
                                                                                                                            .td-header-top-menu
                                                                                                                            .td-icon-search:hover,
                                                                                                                            .td-header-wrap
                                                                                                                            .result-msg
                                                                                                                            a:hover,
                                                                                                                            .top-header-menu
                                                                                                                            li
                                                                                                                            a:hover,
                                                                                                                            .top-header-menu
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .top-header-menu
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .top-header-menu
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-social-icon-wrap
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .td-header-sp-top-widget
                                                                                                                            .td-social-icon-wrap
                                                                                                                            a:hover,
                                                                                                                            .td-page-content
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .td-post-content
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .mce-content-body
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .comment-content
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .wpb_text_column
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .td_block_text_with_title
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .td_module_wrap:hover
                                                                                                                            .entry-title
                                                                                                                            a,
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-list
                                                                                                                            a:hover,
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-dropdown
                                                                                                                            a:hover,
                                                                                                                            .td_quote_on_blocks,
                                                                                                                            .dropcap2,
                                                                                                                            .dropcap3,
                                                                                                                            .td_top_authors
                                                                                                                            .td-active
                                                                                                                            .td-authors-name
                                                                                                                            a,
                                                                                                                            .td_top_authors
                                                                                                                            .td_mod_wrap:hover
                                                                                                                            .td-authors-name
                                                                                                                            a,
                                                                                                                            .td-post-next-prev-content
                                                                                                                            a:hover,
                                                                                                                            .author-box-wrap
                                                                                                                            .td-author-social
                                                                                                                            a:hover,
                                                                                                                            .td-author-name
                                                                                                                            a:hover,
                                                                                                                            .td-author-url
                                                                                                                            a:hover,
                                                                                                                            .td_mod_related_posts:hover
                                                                                                                            h3
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-post-template-11
                                                                                                                            .td-related-title
                                                                                                                            .td-related-left:hover,
                                                                                                                            .td-post-template-11
                                                                                                                            .td-related-title
                                                                                                                            .td-related-right:hover,
                                                                                                                            .td-post-template-11
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td-post-template-11
                                                                                                                            .td_block_related_posts
                                                                                                                            .td-next-prev-wrap
                                                                                                                            a:hover,
                                                                                                                            .comment-reply-link:hover,
                                                                                                                            .logged-in-as
                                                                                                                            a:hover,
                                                                                                                            #cancel-comment-reply-link:hover,
                                                                                                                            .td-search-query,
                                                                                                                            .td-category-header
                                                                                                                            .td-pulldown-category-filter-link:hover,
                                                                                                                            .td-category-siblings
                                                                                                                            .td-subcat-dropdown
                                                                                                                            a:hover,
                                                                                                                            .td-category-siblings
                                                                                                                            .td-subcat-dropdown
                                                                                                                            a.td-current-sub-category,
                                                                                                                            .widget
                                                                                                                            a:hover,
                                                                                                                            .td_wp_recentcomments
                                                                                                                            a:hover,
                                                                                                                            .archive
                                                                                                                            .widget_archive
                                                                                                                            .current,
                                                                                                                            .archive
                                                                                                                            .widget_archive
                                                                                                                            .current
                                                                                                                            a,
                                                                                                                            .widget_calendar
                                                                                                                            tfoot
                                                                                                                            a:hover,
                                                                                                                            .woocommerce
                                                                                                                            a.added_to_cart:hover,
                                                                                                                            .woocommerce-account
                                                                                                                            .woocommerce-MyAccount-navigation
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            li.bbp-header
                                                                                                                            .bbp-reply-content
                                                                                                                            span
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-forum-freshness
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-topic-freshness
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-forums-list
                                                                                                                            li
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-forum-title:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-topic-permalink:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-topic-started-by
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-topic-started-in
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-body
                                                                                                                            .super-sticky
                                                                                                                            li.bbp-topic-title
                                                                                                                            .bbp-topic-permalink,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-body
                                                                                                                            .sticky
                                                                                                                            li.bbp-topic-title
                                                                                                                            .bbp-topic-permalink,
                                                                                                                            .widget_display_replies
                                                                                                                            .bbp-author-name,
                                                                                                                            .widget_display_topics
                                                                                                                            .bbp-author-name,
                                                                                                                            .footer-text-wrap
                                                                                                                            .footer-email-wrap
                                                                                                                            a,
                                                                                                                            .td-subfooter-menu
                                                                                                                            li
                                                                                                                            a:hover,
                                                                                                                            .footer-social-wrap
                                                                                                                            a:hover,
                                                                                                                            a.vc_btn-black:hover,
                                                                                                                            .td-smart-list-dropdown-wrap
                                                                                                                            .td-smart-list-button:hover,
                                                                                                                            .td_module_17
                                                                                                                            .td-read-more
                                                                                                                            a:hover,
                                                                                                                            .td_module_18
                                                                                                                            .td-read-more
                                                                                                                            a:hover,
                                                                                                                            .td_module_19
                                                                                                                            .td-post-author-name
                                                                                                                            a:hover,
                                                                                                                            .td-instagram-user
                                                                                                                            a,
                                                                                                                            .td-pulldown-syle-2
                                                                                                                            .td-subcat-dropdown:hover
                                                                                                                            .td-subcat-more
                                                                                                                            span,
                                                                                                                            .td-pulldown-syle-2
                                                                                                                            .td-subcat-dropdown:hover
                                                                                                                            .td-subcat-more
                                                                                                                            i,
                                                                                                                            .td-pulldown-syle-3
                                                                                                                            .td-subcat-dropdown:hover
                                                                                                                            .td-subcat-more
                                                                                                                            span,
                                                                                                                            .td-pulldown-syle-3
                                                                                                                            .td-subcat-dropdown:hover
                                                                                                                            .td-subcat-more
                                                                                                                            i,
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option:hover,
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option:hover
                                                                                                                            i,
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-link:hover,
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-item
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_2
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_5
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_6
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_7
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_8
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_9
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_10
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_11
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_12
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_13
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_14
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_15
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_16
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td_block_template_17
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .td-theme-wrap
                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .td-menu-item
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .td-theme-wrap
                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td_outlined_btn,
                                                                                                                            .td_block_categories_tags
                                                                                                                            .td-ct-item:hover {
                                                                                                                            color: #703db2;
                                                                                                                        }

                                                                                                                            a.vc_btn-black.vc_btn_square_outlined:hover,
                                                                                                                            a.vc_btn-black.vc_btn_outlined:hover,
                                                                                                                            .td-mega-menu-page
                                                                                                                            .wpb_content_element
                                                                                                                            ul
                                                                                                                            li
                                                                                                                            a:hover,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-aj-search-results
                                                                                                                            .td_module_wrap:hover
                                                                                                                            .entry-title
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .header-search-wrap
                                                                                                                            .result-msg
                                                                                                                            a:hover {
                                                                                                                            color: #703db2 !important;
                                                                                                                        }

                                                                                                                            .td-next-prev-wrap
                                                                                                                            a:hover,
                                                                                                                            .td-load-more-wrap
                                                                                                                            a:hover,
                                                                                                                            .td-post-small-box
                                                                                                                            a:hover,
                                                                                                                            .page-nav
                                                                                                                            .current,
                                                                                                                            .page-nav:first-child
                                                                                                                            >
                                                                                                                            div,
                                                                                                                            .td_category_template_8
                                                                                                                            .td-category-header
                                                                                                                            .td-category
                                                                                                                            a.td-current-sub-category,
                                                                                                                            .td_category_template_4
                                                                                                                            .td-category-siblings
                                                                                                                            .td-category
                                                                                                                            a:hover,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-pagination
                                                                                                                            .current,
                                                                                                                            .post
                                                                                                                            .td_quote_box,
                                                                                                                            .page
                                                                                                                            .td_quote_box,
                                                                                                                            a.vc_btn-black:hover,
                                                                                                                            .td_block_template_5
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td_outlined_btn {
                                                                                                                            border - color: #703db2;
                                                                                                                        }

                                                                                                                            .td_wrapper_video_playlist
                                                                                                                            .td_video_currently_playing:after {
                                                                                                                            border - color: #703db2 !important;
                                                                                                                        }

                                                                                                                            .header-search-wrap
                                                                                                                            .td-drop-down-search:before {
                                                                                                                            border - color: transparent transparent #703db2 transparent;
                                                                                                                        }

                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            span,
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            label,
                                                                                                                            .widgettitle,
                                                                                                                            .widgettitle:after,
                                                                                                                            .td-trending-now-title,
                                                                                                                            .td-trending-now-wrapper:hover
                                                                                                                            .td-trending-now-title,
                                                                                                                            .wpb_tabs
                                                                                                                            li.ui-tabs-active
                                                                                                                            a,
                                                                                                                            .wpb_tabs
                                                                                                                            li:hover
                                                                                                                            a,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container
                                                                                                                            .vc_tta-tab.vc_active
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container
                                                                                                                            .vc_tta-tab:hover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td_block_template_1
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            .products
                                                                                                                            h2:not(.woocommerce-loop-product__title),
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-dropdown:hover
                                                                                                                            .td-subcat-more,
                                                                                                                            .td_3D_btn,
                                                                                                                            .td_shadow_btn,
                                                                                                                            .td_default_btn,
                                                                                                                            .td_round_btn,
                                                                                                                            .td_outlined_btn:hover {
                                                                                                                            background - color: #703db2;
                                                                                                                        }

                                                                                                                            .woocommerce
                                                                                                                            div.product
                                                                                                                            .woocommerce-tabs
                                                                                                                            ul.tabs
                                                                                                                            li.active {
                                                                                                                            background - color: #703db2 !important;
                                                                                                                        }

                                                                                                                            .block-title,
                                                                                                                            .td_block_template_1
                                                                                                                            .td-related-title,
                                                                                                                            .wpb_tabs
                                                                                                                            .wpb_tabs_nav,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container,
                                                                                                                            .woocommerce
                                                                                                                            div.product
                                                                                                                            .woocommerce-tabs
                                                                                                                            ul.tabs:before {
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .td_block_wrap
                                                                                                                            .td-subcat-item
                                                                                                                            a.td-cur-simple-item {
                                                                                                                            color: #703db2;
                                                                                                                        }


                                                                                                                            .td-grid-style-4
                                                                                                                            .entry-title
                                                                                                                            {
                                                                                                                                background - color: rgba(112, 61, 178, 0.7);
                                                                                                                            }


                                                                                                                            .td-theme-wrap
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            span,
                                                                                                                            .td-theme-wrap
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            span
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .widget_rss
                                                                                                                            .block-title
                                                                                                                            .rsswidget,
                                                                                                                            .td-theme-wrap
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .widgettitle,
                                                                                                                            .widgettitle
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-trending-now-title,
                                                                                                                            .wpb_tabs
                                                                                                                            li.ui-tabs-active
                                                                                                                            a,
                                                                                                                            .wpb_tabs
                                                                                                                            li:hover
                                                                                                                            a,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container
                                                                                                                            .vc_tta-tab.vc_active
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container
                                                                                                                            .vc_tta-tab:hover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-related-title
                                                                                                                            .td-cur-simple-item,
                                                                                                                            .woocommerce
                                                                                                                            div.product
                                                                                                                            .woocommerce-tabs
                                                                                                                            ul.tabs
                                                                                                                            li.active,
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            .products
                                                                                                                            h2:not(.woocommerce-loop-product__title),
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_2
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_3
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_4
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_5
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_6
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_6
                                                                                                                            .td-block-title:before,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_7
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_8
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_9
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_10
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_11
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_12
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_13
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            span,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_13
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_14
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_14
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_14
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option
                                                                                                                            i,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_14
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option:hover,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_14
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option:hover
                                                                                                                            i,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_15
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_15
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_15
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            i,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_16
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            *,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td_block_template_17
                                                                                                                            .td-block-title
                                                                                                                            >
                                                                                                                            * {
                                                                                                                            color: #444444;
                                                                                                                        }


                                                                                                                            .td-header-wrap
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-header-wrap
                                                                                                                            .td-header-menu-social
                                                                                                                            .td-social-icon-wrap
                                                                                                                            a,
                                                                                                                            .td-header-style-4
                                                                                                                            .td-header-menu-social
                                                                                                                            .td-social-icon-wrap
                                                                                                                            i,
                                                                                                                            .td-header-style-5
                                                                                                                            .td-header-menu-social
                                                                                                                            .td-social-icon-wrap
                                                                                                                            i,
                                                                                                                            .td-header-style-6
                                                                                                                            .td-header-menu-social
                                                                                                                            .td-social-icon-wrap
                                                                                                                            i,
                                                                                                                            .td-header-style-12
                                                                                                                            .td-header-menu-social
                                                                                                                            .td-social-icon-wrap
                                                                                                                            i,
                                                                                                                            .td-header-wrap
                                                                                                                            .header-search-wrap
                                                                                                                            #td-header-search-button
                                                                                                                            .td-icon-search {
                                                                                                                            color: #444444;
                                                                                                                        }
                                                                                                                            .td-header-wrap
                                                                                                                            .td-header-menu-social
                                                                                                                            +
                                                                                                                            .td-search-wrapper
                                                                                                                            #td-header-search-button:before {
                                                                                                                            background - color: #444444;
                                                                                                                        }


                                                                                                                            .td-menu-background:before,
                                                                                                                            .td-search-background:before {
                                                                                                                            background: rgba(0,0,0,0.8);
                                                                                                                            background: -moz-linear-gradient(top, rgba(0,0,0,0.8) 0%, rgba(0,0,0,0.7) 100%);
                                                                                                                            background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0,0,0,0.8)), color-stop(100%, rgba(0,0,0,0.7)));
                                                                                                                            background: -webkit-linear-gradient(top, rgba(0,0,0,0.8) 0%, rgba(0,0,0,0.7) 100%);
                                                                                                                            background: -o-linear-gradient(top, rgba(0,0,0,0.8) 0%, rgba(0,0,0,0.7) 100%);
                                                                                                                            background: -ms-linear-gradient(top, rgba(0,0,0,0.8) 0%, rgba(0,0,0,0.7) 100%);
                                                                                                                            background: linear-gradient(to bottom, rgba(0,0,0,0.8) 0%, rgba(0,0,0,0.7) 100%);
                                                                                                                            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='rgba(0,0,0,0.8)', endColorstr='rgba(0,0,0,0.7)', GradientType=0 );
                                                                                                                        }


                                                                                                                            .td-mobile-content
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-mobile-content
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-mobile-content
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            #td-mobile-nav
                                                                                                                            .td-menu-login-section
                                                                                                                            a:hover,
                                                                                                                            #td-mobile-nav
                                                                                                                            .td-register-section
                                                                                                                            a:hover,
                                                                                                                            #td-mobile-nav
                                                                                                                            .td-menu-socials-wrap
                                                                                                                            a:hover
                                                                                                                            i,
                                                                                                                            .td-search-close
                                                                                                                            a:hover
                                                                                                                            i {
                                                                                                                            color: #a39176;
                                                                                                                        }


                                                                                                                            .td-header-bg:before {
                                                                                                                            background - size: auto;
                                                                                                                        }


                                                                                                                            .td-header-bg:before {
                                                                                                                            background - position: center center;
                                                                                                                        }


                                                                                                                            .post
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            color: #444444;
                                                                                                                        }
                                                                                                                            .td_module_15
                                                                                                                            .entry-title
                                                                                                                            a {
                                                                                                                            color: #444444;
                                                                                                                        }


                                                                                                                            .td-module-meta-info
                                                                                                                            .td-post-author-name
                                                                                                                            a {
                                                                                                                            color: #444444;
                                                                                                                        }


                                                                                                                            .td-post-content
                                                                                                                            h1,
                                                                                                                            .td-post-content
                                                                                                                            h2,
                                                                                                                            .td-post-content
                                                                                                                            h3,
                                                                                                                            .td-post-content
                                                                                                                            h4,
                                                                                                                            .td-post-content
                                                                                                                            h5,
                                                                                                                            .td-post-content
                                                                                                                            h6 {
                                                                                                                            color: #503771;
                                                                                                                        }


                                                                                                                            .post
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .page
                                                                                                                            blockquote
                                                                                                                            p {
                                                                                                                            color: #503771;
                                                                                                                        }
                                                                                                                            .post
                                                                                                                            .td_quote_box,
                                                                                                                            .page
                                                                                                                            .td_quote_box {
                                                                                                                            border - color: #503771;
                                                                                                                        }


                                                                                                                            .td-page-header
                                                                                                                            h1,
                                                                                                                            .td-page-title,
                                                                                                                            .woocommerce-page
                                                                                                                            .page-title {
                                                                                                                            color: #444444;
                                                                                                                        }


                                                                                                                            .td-page-content
                                                                                                                            h1,
                                                                                                                            .td-page-content
                                                                                                                            h2,
                                                                                                                            .td-page-content
                                                                                                                            h3,
                                                                                                                            .td-page-content
                                                                                                                            h4,
                                                                                                                            .td-page-content
                                                                                                                            h5,
                                                                                                                            .td-page-content
                                                                                                                            h6 {
                                                                                                                            color: #444444;
                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            .widgettitle {
                                                                                                                            color: #fff;
                                                                                                                        }


                                                                                                                            .td-menu-background,
                                                                                                                            .td-search-background {
                                                                                                                            background - image: url('https://koolkanya.com/blogs/wp-content/uploads/2019/05/cover-image.jpg');
                                                                                                                        }


                                                                                                                            .top-header-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-weather-top-widget
                                                                                                                            .td-weather-now
                                                                                                                            .td-big-degrees,
                                                                                                                            .td-weather-top-widget
                                                                                                                            .td-weather-header
                                                                                                                            .td-weather-city,
                                                                                                                            .td-header-sp-top-menu
                                                                                                                            .td_data_time {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            font-size:15px;
                                                                                                                            text-transform:none;

                                                                                                                        }

                                                                                                                            .top-header-menu
                                                                                                                            .menu-item-has-children
                                                                                                                            li
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-weight:normal;

                                                                                                                        }

                                                                                                                            ul.sf-menu
                                                                                                                            >
                                                                                                                            .td-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-header-menu-social {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            font-size:15px;
                                                                                                                            text-transform:none;

                                                                                                                        }

                                                                                                                            .sf-menu
                                                                                                                            ul
                                                                                                                            .td-menu-item
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_mod_mega_menu
                                                                                                                            .item-details
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:15px;
                                                                                                                            text-transform:none;

                                                                                                                        }

                                                                                                                            .td_mega_menu_sub_cats
                                                                                                                            .block-mega-child-cats
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-mobile-content
                                                                                                                            .td-mobile-main-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-mobile-content
                                                                                                                            .sub-menu
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-header-wrap
                                                                                                                            .td-logo-text-container
                                                                                                                            .td-logo-text {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-header-wrap
                                                                                                                            .td-logo-text-container
                                                                                                                            .td-tagline-text {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            span,
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .widgettitle,
                                                                                                                            .td-trending-now-title,
                                                                                                                            .wpb_tabs
                                                                                                                            li
                                                                                                                            a,
                                                                                                                            .vc_tta-container
                                                                                                                            .vc_tta-color-grey.vc_tta-tabs-position-top.vc_tta-style-classic
                                                                                                                            .vc_tta-tabs-container
                                                                                                                            .vc_tta-tab
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-related-title
                                                                                                                            a,
                                                                                                                            .woocommerce
                                                                                                                            div.product
                                                                                                                            .woocommerce-tabs
                                                                                                                            ul.tabs
                                                                                                                            li
                                                                                                                            a,
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            .products
                                                                                                                            h2:not(.woocommerce-loop-product__title),
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-block-title {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:20px;
                                                                                                                            line-height:50px;
                                                                                                                            font-weight:bold;

                                                                                                                        }

                                                                                                                            .td-theme-wrap
                                                                                                                            .td-subcat-filter,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-dropdown,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-block-title-wrap
                                                                                                                            .td-wrapper-pulldown-filter
                                                                                                                            .td-pulldown-filter-display-option,
                                                                                                                            .td-theme-wrap
                                                                                                                            .td-pulldown-category {
                                                                                                                            line - height: 50px;
                                                                                                                        }
                                                                                                                            .td_block_template_1
                                                                                                                            .block-title
                                                                                                                            >
                                                                                                                            * {
                                                                                                                            padding - bottom: 0;
                                                                                                                            padding-top: 0;
                                                                                                                        }

                                                                                                                            .td_module_wrap
                                                                                                                            .td-post-author-name
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-date
                                                                                                                            .entry-date {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-module-comments
                                                                                                                            a,
                                                                                                                            .td-post-views
                                                                                                                            span,
                                                                                                                            .td-post-comments
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-big-grid-meta
                                                                                                                            .td-post-category,
                                                                                                                            .td_module_wrap
                                                                                                                            .td-post-category,
                                                                                                                            .td-module-image
                                                                                                                            .td-post-category {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-dropdown
                                                                                                                            a,
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-list
                                                                                                                            a,
                                                                                                                            .td-subcat-filter
                                                                                                                            .td-subcat-dropdown
                                                                                                                            span {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-excerpt,
                                                                                                                            .td_module_14
                                                                                                                            .td-excerpt {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }


                                                                                                                            .td_module_wrap
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_1
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_2
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_3
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_4
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_5
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_6
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_7
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_8
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_9
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_10
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_11
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_12
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_13
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_14
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_15
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_16
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_17
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_18
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:48px;
                                                                                                                            line-height:54px;
                                                                                                                            font-weight:normal;

                                                                                                                        }

                                                                                                                            .td_module_19
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }


                                                                                                                            .td_block_trending_now
                                                                                                                            .entry-title,
                                                                                                                            .td-theme-slider
                                                                                                                            .td-module-title,
                                                                                                                            .td-big-grid-post
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx1
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx2
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx3
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx4
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx5
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx6
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx7
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx8
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx9
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx10
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx11
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx12
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx13
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx14
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx15
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx16
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx17
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx18
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:48px;
                                                                                                                            line-height:54px;
                                                                                                                            font-weight:normal;

                                                                                                                        }

                                                                                                                            .td_module_mx19
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx20
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx21
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx22
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx23
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx24
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx25
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_module_mx26
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_block_trending_now
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-theme-slider.iosSlider-col-1
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-theme-slider.iosSlider-col-2
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-theme-slider.iosSlider-col-3
                                                                                                                            .td-module-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-big-grid-post.td-big-thumb
                                                                                                                            .td-big-grid-meta,
                                                                                                                            .td-big-thumb
                                                                                                                            .td-big-grid-meta
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-big-grid-post.td-medium-thumb
                                                                                                                            .td-big-grid-meta,
                                                                                                                            .td-medium-thumb
                                                                                                                            .td-big-grid-meta
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-big-grid-post.td-small-thumb
                                                                                                                            .td-big-grid-meta,
                                                                                                                            .td-small-thumb
                                                                                                                            .td-big-grid-meta
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-big-grid-post.td-tiny-thumb
                                                                                                                            .td-big-grid-meta,
                                                                                                                            .td-tiny-thumb
                                                                                                                            .td-big-grid-meta
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .homepage-post
                                                                                                                            .td-post-template-8
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            #td-mobile-nav,
                                                                                                                            #td-mobile-nav
                                                                                                                            .wpb_button,
                                                                                                                            .td-search-wrap-mob {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-default
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-1
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-2
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-3
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-4
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-5
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-6
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-7
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-8
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-9
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-10
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-11
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-12
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-13
                                                                                                                            .td-post-header
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            p,
                                                                                                                            .td-post-content {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:16px;
                                                                                                                            font-weight:normal;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .page
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .td-post-text-content
                                                                                                                            blockquote
                                                                                                                            p {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:18px;
                                                                                                                            font-style:normal;
                                                                                                                            font-weight:normal;
                                                                                                                            text-transform:none;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td_quote_box
                                                                                                                            p,
                                                                                                                            .page
                                                                                                                            .td_quote_box
                                                                                                                            p {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            text-transform:none;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td_pull_quote
                                                                                                                            p,
                                                                                                                            .page
                                                                                                                            .td_pull_quote
                                                                                                                            p,
                                                                                                                            .post
                                                                                                                            .wp-block-pullquote
                                                                                                                            blockquote
                                                                                                                            p,
                                                                                                                            .page
                                                                                                                            .wp-block-pullquote
                                                                                                                            blockquote
                                                                                                                            p {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            li {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h1 {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            font-size:32px;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h2 {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            font-size:26px;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h3 {
                                                                                                                            font - family:SourceSansPro-Regular;
                                                                                                                            font-size:22px;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h4 {
                                                                                                                            font - family:SourceSansPro-Regular;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h5 {
                                                                                                                            font - family:SourceSansPro-Regular;

                                                                                                                        }

                                                                                                                            .td-post-content
                                                                                                                            h6 {
                                                                                                                            font - family:SourceSansPro-Regular;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-category
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            header
                                                                                                                            .td-post-author-name,
                                                                                                                            .post
                                                                                                                            header
                                                                                                                            .td-post-author-name
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            header
                                                                                                                            .td-post-date
                                                                                                                            .entry-date {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            header
                                                                                                                            .td-post-views
                                                                                                                            span,
                                                                                                                            .post
                                                                                                                            header
                                                                                                                            .td-post-comments {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-post-source-tags
                                                                                                                            a,
                                                                                                                            .post
                                                                                                                            .td-post-source-tags
                                                                                                                            span {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-post-next-prev-content
                                                                                                                            span {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-post-next-prev-content
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:16px;
                                                                                                                            line-height:20px;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .author-box-wrap
                                                                                                                            .td-author-name
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .author-box-wrap
                                                                                                                            .td-author-url
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .author-box-wrap
                                                                                                                            .td-author-description {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td_block_related_posts
                                                                                                                            .entry-title
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;
                                                                                                                            font-size:16px;
                                                                                                                            line-height:20px;

                                                                                                                        }

                                                                                                                            .post
                                                                                                                            .td-post-share-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .wp-caption-text,
                                                                                                                            .wp-caption-dd,
                                                                                                                            .wp-block-image
                                                                                                                            figcaption {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-default
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-1
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-4
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-5
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-9
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-10
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-11
                                                                                                                            .td-post-sub-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-post-template-2
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-3
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-6
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-7
                                                                                                                            .td-post-sub-title,
                                                                                                                            .td-post-template-8
                                                                                                                            .td-post-sub-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-title,
                                                                                                                            .woocommerce-page
                                                                                                                            .page-title,
                                                                                                                            .td-category-title-holder
                                                                                                                            .td-page-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            p,
                                                                                                                            .td-page-content
                                                                                                                            li,
                                                                                                                            .td-page-content
                                                                                                                            .td_block_text_with_title,
                                                                                                                            .woocommerce-page
                                                                                                                            .page-description
                                                                                                                            >
                                                                                                                            p,
                                                                                                                            .wpb_text_column
                                                                                                                            p {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h1,
                                                                                                                            .wpb_text_column
                                                                                                                            h1 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h2,
                                                                                                                            .wpb_text_column
                                                                                                                            h2 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h3,
                                                                                                                            .wpb_text_column
                                                                                                                            h3 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h4,
                                                                                                                            .wpb_text_column
                                                                                                                            h4 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h5,
                                                                                                                            .wpb_text_column
                                                                                                                            h5 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-page-content
                                                                                                                            h6,
                                                                                                                            .wpb_text_column
                                                                                                                            h6 {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .footer-text-wrap {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-sub-footer-copy {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-sub-footer-menu
                                                                                                                            ul
                                                                                                                            li
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .entry-crumbs
                                                                                                                            a,
                                                                                                                            .entry-crumbs
                                                                                                                            span,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-breadcrumb
                                                                                                                            a,
                                                                                                                            #bbpress-forums
                                                                                                                            .bbp-breadcrumb
                                                                                                                            .bbp-breadcrumb-current {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .category
                                                                                                                            .td-category
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .td-trending-now-display-area
                                                                                                                            .entry-title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }

                                                                                                                            .page-nav
                                                                                                                            a,
                                                                                                                            .page-nav
                                                                                                                            span {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            #td-outer-wrap
                                                                                                                            span.dropcap,
                                                                                                                            #td-outer-wrap
                                                                                                                            p.has-drop-cap:not(:focus)::first-letter {
                                                                                                                            font - family:SourceSansPro-Regular;

                                                                                                                        }
                                                                                                                            .widget_archive
                                                                                                                            a,
                                                                                                                            .widget_calendar,
                                                                                                                            .widget_categories
                                                                                                                            a,
                                                                                                                            .widget_nav_menu
                                                                                                                            a,
                                                                                                                            .widget_meta
                                                                                                                            a,
                                                                                                                            .widget_pages
                                                                                                                            a,
                                                                                                                            .widget_recent_comments
                                                                                                                            a,
                                                                                                                            .widget_recent_entries
                                                                                                                            a,
                                                                                                                            .widget_text
                                                                                                                            .textwidget,
                                                                                                                            .widget_tag_cloud
                                                                                                                            a,
                                                                                                                            .widget_search
                                                                                                                            input,
                                                                                                                            .woocommerce
                                                                                                                            .product-categories
                                                                                                                            a,
                                                                                                                            .widget_display_forums
                                                                                                                            a,
                                                                                                                            .widget_display_replies
                                                                                                                            a,
                                                                                                                            .widget_display_topics
                                                                                                                            a,
                                                                                                                            .widget_display_views
                                                                                                                            a,
                                                                                                                            .widget_display_stats {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            input[type="submit"],
                                                                                                                            .td-read-more
                                                                                                                            a,
                                                                                                                            .vc_btn,
                                                                                                                            .woocommerce
                                                                                                                            a.button,
                                                                                                                            .woocommerce
                                                                                                                            button.button,
                                                                                                                            .woocommerce
                                                                                                                            #respond
                                                                                                                            input#submit {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            a
                                                                                                                            .woocommerce-loop-product__title,
                                                                                                                            .woocommerce
                                                                                                                            .widget.woocommerce
                                                                                                                            .product_list_widget
                                                                                                                            a,
                                                                                                                            .woocommerce-cart
                                                                                                                            .woocommerce
                                                                                                                            .product-name
                                                                                                                            a {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            .woocommerce
                                                                                                                            .product
                                                                                                                            .summary
                                                                                                                            .product_title {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            .white-popup-block,
                                                                                                                            .white-popup-block
                                                                                                                            .wpb_button {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            body,
                                                                                                                            p {
                                                                                                                            font - family:FFTisaOT;

                                                                                                                        }
                                                                                                                            /* Style generated by theme for demo: blog_coffee */

                                                                                                                            .td-blog-coffee
                                                                                                                            .td-header-style-7
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .td-blog-coffee
                                                                                                                            .td-header-style-7
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-blog-coffee
                                                                                                                            .td-header-style-7
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-blog-coffee
                                                                                                                            .td-header-style-7
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .td-blog-coffee
                                                                                                                            .td-header-style-7
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                        </style>

                                                                                                                        <style
                                                                                                                            type="text/css"
                                                                                                                            id="wp-custom-css">
                                                                                                                            /* Fix homepage post images */
                                                                                                                            .td_module_mx1
                                                                                                                            .td-module-thumb {
                                                                                                                            height: auto;
                                                                                                                        }
                                                                                                                            /* Homepage recent article and about us header size */
                                                                                                                            .td_block_fancy_text
                                                                                                                            .tdm-fancy-title {
                                                                                                                            font - family: Gotham-Bold;
                                                                                                                            font-size: 80px;
                                                                                                                            line-height: 56px;
                                                                                                                        }

                                                                                                                            /* Fix logo */
                                                                                                                            @media
                                                                                                                            (max-width:
                                                                                                                            1018px)
                                                                                                                            and
                                                                                                                            (min-width:
                                                                                                                            768px) {
                                                                                                                            .td-header-style-7 .td-header-sp-logo img{
                                                                                                                            max-width:250px;
                                                                                                                        }
                                                                                                                        }

                                                                                                                            @media
                                                                                                                            (min-width:
                                                                                                                            767px){
                                                                                                                            .td-blog-coffee .td-header-style-7 {
                                                                                                                            padding-top: 16px;
                                                                                                                            padding-bottom: 16px;
                                                                                                                        }
                                                                                                                        }        </style>
                                                                                                                        <style
                                                                                                                            type="text/css"
                                                                                                                            title="dynamic-css"
                                                                                                                            className="options-output">.wtr-time-wrap{line - height:16px;color:#000000;font-size:16px;opacity: 1;visibility: visible;-webkit-transition: opacity 0.24s ease-in-out;-moz-transition: opacity 0.24s ease-in-out;transition: opacity 0.24s ease-in-out;}.wf-loading
                                                                                                                            .wtr-time-wrap,{opacity: 0;}.ie.wf-loading
                                                                                                                            .wtr-time-wrap,{visibility: hidden;}</style>
                                                                                                                        <!-- Button style compiled by theme -->

                                                                                                                        <style>
                                                                                                                            .tdm-menu-active-style3
                                                                                                                            .tdm-header.td-header-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style3
                                                                                                                            .tdm-header.td-header-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style3
                                                                                                                            .tdm-header.td-header-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style3
                                                                                                                            .tdm-header.td-header-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style3
                                                                                                                            .tdm-header.td-header-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .tdm_block_column_content:hover
                                                                                                                            .tdm-col-content-title-url
                                                                                                                            .tdm-title,
                                                                                                                            .tds-button2
                                                                                                                            .tdm-btn-text,
                                                                                                                            .tds-button2
                                                                                                                            i,
                                                                                                                            .tds-button5:hover
                                                                                                                            .tdm-btn-text,
                                                                                                                            .tds-button5:hover
                                                                                                                            i,
                                                                                                                            .tds-button6
                                                                                                                            .tdm-btn-text,
                                                                                                                            .tds-button6
                                                                                                                            i,
                                                                                                                            .tdm_block_list
                                                                                                                            .tdm-list-item
                                                                                                                            i,
                                                                                                                            .tdm_block_pricing
                                                                                                                            .tdm-pricing-feature
                                                                                                                            i,
                                                                                                                            .tdm-social-item
                                                                                                                            i {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-active-style5
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style5
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style5
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style5
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .tdm-menu-active-style5
                                                                                                                            .td-header-menu-wrap
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tds-button1,
                                                                                                                            .tds-button6:after,
                                                                                                                            .tds-title2
                                                                                                                            .tdm-title-line:after,
                                                                                                                            .tds-title3
                                                                                                                            .tdm-title-line:after,
                                                                                                                            .tdm_block_pricing.tdm-pricing-featured:before,
                                                                                                                            .tdm_block_pricing.tds_pricing2_block.tdm-pricing-featured
                                                                                                                            .tdm-pricing-header,
                                                                                                                            .tds-progress-bar1
                                                                                                                            .tdm-progress-bar:after,
                                                                                                                            .tds-progress-bar2
                                                                                                                            .tdm-progress-bar:after,
                                                                                                                            .tds-social3
                                                                                                                            .tdm-social-item {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-active-style4
                                                                                                                            .tdm-header
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-item
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style4
                                                                                                                            .tdm-header
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-menu-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style4
                                                                                                                            .tdm-header
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .current-category-ancestor
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tdm-menu-active-style4
                                                                                                                            .tdm-header
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            li
                                                                                                                            >
                                                                                                                            a:hover,
                                                                                                                            .tdm-menu-active-style4
                                                                                                                            .tdm-header
                                                                                                                            .sf-menu
                                                                                                                            >
                                                                                                                            .sfHover
                                                                                                                            >
                                                                                                                            a,
                                                                                                                            .tds-button2:before,
                                                                                                                            .tds-button6:before,
                                                                                                                            .tds-progress-bar3
                                                                                                                            .tdm-progress-bar:after {
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-btn-style1 {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-btn-style2:before {
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-btn-style2 {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-btn-style3 {
                                                                                                                            -webkit - box - shadow: 0 2px 16px #703db2;
                                                                                                                            -moz-box-shadow: 0 2px 16px #703db2;
                                                                                                                            box-shadow: 0 2px 16px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-btn-style3:hover {
                                                                                                                            -webkit - box - shadow: 0 4px 26px #703db2;
                                                                                                                            -moz-box-shadow: 0 4px 26px #703db2;
                                                                                                                            box-shadow: 0 4px 26px #703db2;
                                                                                                                        }


                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button1,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button6:after {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button2:before,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button6:before {
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button2,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button2
                                                                                                                            i {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button3 {
                                                                                                                            -webkit - box - shadow: 0 2px 16px #703db2;
                                                                                                                            -moz-box-shadow: 0 2px 16px #703db2;
                                                                                                                            box-shadow: 0 2px 16px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button3:hover {
                                                                                                                            -webkit - box - shadow: 0 4px 26px #703db2;
                                                                                                                            -moz-box-shadow: 0 4px 26px #703db2;
                                                                                                                            box-shadow: 0 4px 26px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button7
                                                                                                                            .tdm-btn-border-top,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button7
                                                                                                                            .tdm-btn-border-bottom {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button8 {
                                                                                                                            background: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button1:before,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button4
                                                                                                                            .tdm-button-b {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button2:hover:before,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button6:hover:before{
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tdm-btn-style:hover {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button3:hover {
                                                                                                                            -webkit - box - shadow: 0 4px 26px #703db2;
                                                                                                                            -moz-box-shadow: 0 4px 26px #703db2;
                                                                                                                            box-shadow: 0 4px 26px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button7:hover
                                                                                                                            .tdm-btn-border-top,
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button7:hover
                                                                                                                            .tdm-btn-border-bottom {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn1
                                                                                                                            .tds-button8:before {
                                                                                                                            background - color: #703db2;
                                                                                                                        }


                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button1,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button6:after {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button2:before,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button6:before {
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button2,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button2
                                                                                                                            i {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button3 {
                                                                                                                            -webkit - box - shadow: 0 2px 16px #703db2;
                                                                                                                            -moz-box-shadow: 0 2px 16px #703db2;
                                                                                                                            box-shadow: 0 2px 16px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button3:hover {
                                                                                                                            -webkit - box - shadow: 0 4px 26px #703db2;
                                                                                                                            -moz-box-shadow: 0 4px 26px #703db2;
                                                                                                                            box-shadow: 0 4px 26px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button7
                                                                                                                            .tdm-btn-border-top,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button7
                                                                                                                            .tdm-btn-border-bottom {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button8 {
                                                                                                                            background: #703db2;
                                                                                                                        }


                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button1:before,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button4
                                                                                                                            .tdm-button-b {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button2:hover:before,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button6:hover:before{
                                                                                                                            border - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tdm-btn-style:hover {
                                                                                                                            color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button3:hover {
                                                                                                                            -webkit - box - shadow: 0 4px 26px #703db2;
                                                                                                                            -moz-box-shadow: 0 4px 26px #703db2;
                                                                                                                            box-shadow: 0 4px 26px #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button7:hover
                                                                                                                            .tdm-btn-border-top,
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button7:hover
                                                                                                                            .tdm-btn-border-bottom {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                            .tdm-menu-btn2
                                                                                                                            .tds-button8:before {
                                                                                                                            background - color: #703db2;
                                                                                                                        }
                                                                                                                        </style>

                                                                                                                        <noscript>
                                                                                                                            <style
                                                                                                                                type="text/css"> .wpb_animate_when_almost_visible {opacity: 1;}</style>
                                                                                                                        </noscript>
                                                                                                                        <style
                                                                                                                            id="tdw-css-placeholder">.td-main-content-wrap.td-main-page-wrap.td-container-wrap {
                                                                                                                            padding - top: 0px;
                                                                                                                        }
                                                                                                                            .td_block_fancy_text
                                                                                                                            .tdm-fancy-title2 {
                                                                                                                                color: #503771;
                                                                                                                            }
                                                                                                                            .td_module_flex_2
                                                                                                                            .td-module-comments
                                                                                                                            a {
                                                                                                                                display: none;
                                                                                                                            }
                                                                                                                            .td_module_wrap
                                                                                                                            .td-post-author-name
                                                                                                                            a {
                                                                                                                                color: #fff;
                                                                                                                            }
                                                                                                                            .topmob {
                                                                                                                                font - family: SourceSansPro-Regular !important;
                                                                                                                                color: #fff;
                                                                                                                                margin-bottom: 7px;
                                                                                                                                line-height: 2px;
                                                                                                                                font-size: 14px !important;
                                                                                                                            }
                                                                                                                            .td-post-date
                                                                                                                            .entry-date {
                                                                                                                                color: #aaa;
                                                                                                                            }
                                                                                                                            div.two-col-articles.td_block_14
                                                                                                                            div.td-block-span4 {
                                                                                                                                width: 50%;
                                                                                                                            }
                                                                                                                            .td_block_wrap.td_block_14.td_uid_16_5b9539d7bf606_rand.td-pb-full-cell.td-pb-border-top.two-col-articles.td_block_template_9.td-column-3 {
                                                                                                                                margin - bottom: 0px;
                                                                                                                            }
                                                                                                                        </style>
                                                                                                                        <script
                                                                                                                            src="https://koolkanya.com/blogs/wp-includes/js/wp-emoji-release.min.js?ver=5.2.2"
                                                                                                                            type="text/javascript"
                                                                                                                            defer=""></script>
                                                                                                                        <script
                                                                                                                            async=""
                                                                                                                            src="https://static.hotjar.com/c/hotjar-1313190.js?sv=6"></script>
                                                                                                                        <script
                                                                                                                            charSet="utf-8"
                                                                                                                            src="https://assets.ubembed.com/universalscript/releases/v0.177.0/bundle.js"></script>
                                                                                                                        <style
                                                                                                                            id="ace_editor.css">.ace_editor {position: relative;overflow: hidden;font: 12px/normal 'Monaco', 'Menlo', 'Ubuntu Mono', 'Consolas', 'source-code-pro', monospace;direction: ltr;}.ace_scroller {position: absolute;overflow: hidden;top: 0;bottom: 0;background-color: inherit;-ms-user-select: none;-moz-user-select: none;-webkit-user-select: none;user-select: none;cursor: text;}.ace_content {position: absolute;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;min-width: 100%;}.ace_dragging
                                                                                                                            .ace_scroller:before{position: absolute;top: 0;left: 0;right: 0;bottom: 0;content: '';background: rgba(250, 250, 250, 0.01);z-index: 1000;}.ace_dragging.ace_dark
                                                                                                                            .ace_scroller:before{background: rgba(0, 0, 0, 0.01);}.ace_selecting,
                                                                                                                            .ace_selecting
                                                                                                                            * {cursor: text !important;}.ace_gutter {position: absolute;overflow : hidden;width: auto;top: 0;bottom: 0;left: 0;cursor: default;z-index: 4;-ms-user-select: none;-moz-user-select: none;-webkit-user-select: none;user-select: none;}.ace_gutter-active-line {position: absolute;left: 0;right: 0;}.ace_scroller.ace_scroll-left {box - shadow: 17px 0 16px -16px rgba(0, 0, 0, 0.4) inset;}.ace_gutter-cell {padding - left: 19px;padding-right: 6px;background-repeat: no-repeat;}.ace_gutter-cell.ace_error {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAABOFBMVEX/////////QRswFAb/Ui4wFAYwFAYwFAaWGAfDRymzOSH/PxswFAb/SiUwFAYwFAbUPRvjQiDllog5HhHdRybsTi3/Tyv9Tir+Syj/UC3////XurebMBIwFAb/RSHbPx/gUzfdwL3kzMivKBAwFAbbvbnhPx66NhowFAYwFAaZJg8wFAaxKBDZurf/RB6mMxb/SCMwFAYwFAbxQB3+RB4wFAb/Qhy4Oh+4QifbNRcwFAYwFAYwFAb/QRzdNhgwFAYwFAbav7v/Uy7oaE68MBK5LxLewr/r2NXewLswFAaxJw4wFAbkPRy2PyYwFAaxKhLm1tMwFAazPiQwFAaUGAb/QBrfOx3bvrv/VC/maE4wFAbRPBq6MRO8Qynew8Dp2tjfwb0wFAbx6eju5+by6uns4uH9/f36+vr/GkHjAAAAYnRSTlMAGt+64rnWu/bo8eAA4InH3+DwoN7j4eLi4xP99Nfg4+b+/u9B/eDs1MD1mO7+4PHg2MXa347g7vDizMLN4eG+Pv7i5evs/v79yu7S3/DV7/498Yv24eH+4ufQ3Ozu/v7+y13sRqwAAADLSURBVHjaZc/XDsFgGIBhtDrshlitmk2IrbHFqL2pvXf/+78DPokj7+Fz9qpU/9UXJIlhmPaTaQ6QPaz0mm+5gwkgovcV6GZzd5JtCQwgsxoHOvJO15kleRLAnMgHFIESUEPmawB9ngmelTtipwwfASilxOLyiV5UVUyVAfbG0cCPHig+GBkzAENHS0AstVF6bacZIOzgLmxsHbt2OecNgJC83JERmePUYq8ARGkJx6XtFsdddBQgZE2nPR6CICZhawjA4Fb/chv+399kfR+MMMDGOQAAAABJRU5ErkJggg==");background-repeat: no-repeat;background-position: 2px center;}.ace_gutter-cell.ace_warning {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAmVBMVEX///8AAAD///8AAAAAAABPSzb/5sAAAAB/blH/73z/ulkAAAAAAAD85pkAAAAAAAACAgP/vGz/rkDerGbGrV7/pkQICAf////e0IsAAAD/oED/qTvhrnUAAAD/yHD/njcAAADuv2r/nz//oTj/p064oGf/zHAAAAA9Nir/tFIAAAD/tlTiuWf/tkIAAACynXEAAAAAAAAtIRW7zBpBAAAAM3RSTlMAABR1m7RXO8Ln31Z36zT+neXe5OzooRDfn+TZ4p3h2hTf4t3k3ucyrN1K5+Xaks52Sfs9CXgrAAAAjklEQVR42o3PbQ+CIBQFYEwboPhSYgoYunIqqLn6/z8uYdH8Vmdnu9vz4WwXgN/xTPRD2+sgOcZjsge/whXZgUaYYvT8QnuJaUrjrHUQreGczuEafQCO/SJTufTbroWsPgsllVhq3wJEk2jUSzX3CUEDJC84707djRc5MTAQxoLgupWRwW6UB5fS++NV8AbOZgnsC7BpEAAAAABJRU5ErkJggg==");background-position: 2px center;}.ace_gutter-cell.ace_info {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAJ0Uk5TAAB2k804AAAAPklEQVQY02NgIB68QuO3tiLznjAwpKTgNyDbMegwisCHZUETUZV0ZqOquBpXj2rtnpSJT1AEnnRmL2OgGgAAIKkRQap2htgAAAAASUVORK5CYII=");background-position: 2px center;}.ace_dark
                                                                                                                            .ace_gutter-cell.ace_info {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAJFBMVEUAAAChoaGAgIAqKiq+vr6tra1ZWVmUlJSbm5s8PDxubm56enrdgzg3AAAAAXRSTlMAQObYZgAAAClJREFUeNpjYMAPdsMYHegyJZFQBlsUlMFVCWUYKkAZMxZAGdxlDMQBAG+TBP4B6RyJAAAAAElFTkSuQmCC");}.ace_scrollbar {position: absolute;right: 0;bottom: 0;z-index: 6;}.ace_scrollbar-inner {position: absolute;cursor: text;left: 0;top: 0;}.ace_scrollbar-v{overflow - x: hidden;overflow-y: scroll;top: 0;}.ace_scrollbar-h {overflow - x: scroll;overflow-y: hidden;left: 0;}.ace_print-margin {position: absolute;height: 100%;}.ace_text-input {position: absolute;z-index: 0;width: 0.5em;height: 1em;opacity: 0;background: transparent;-moz-appearance: none;appearance: none;border: none;resize: none;outline: none;overflow: hidden;font: inherit;padding: 0 1px;margin: 0 -1px;text-indent: -1em;-ms-user-select: text;-moz-user-select: text;-webkit-user-select: text;user-select: text;white-space: pre!important;}.ace_text-input.ace_composition {background: inherit;color: inherit;z-index: 1000;opacity: 1;text-indent: 0;}.ace_layer {z - index: 1;position: absolute;overflow: hidden;word-wrap: normal;white-space: pre;height: 100%;width: 100%;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;pointer-events: none;}.ace_gutter-layer {position: relative;width: auto;text-align: right;pointer-events: auto;}.ace_text-layer {font: inherit !important;}.ace_cjk {display: inline-block;text-align: center;}.ace_cursor-layer {z - index: 4;}.ace_cursor {z - index: 4;position: absolute;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;border-left: 2px solid;transform: translatez(0);}.ace_slim-cursors
                                                                                                                            .ace_cursor {border - left - width: 1px;}.ace_overwrite-cursors
                                                                                                                            .ace_cursor {border - left - width: 0;border-bottom: 1px solid;}.ace_hidden-cursors
                                                                                                                            .ace_cursor {opacity: 0.2;}.ace_smooth-blinking
                                                                                                                            .ace_cursor {-webkit - transition: opacity 0.18s;transition: opacity 0.18s;}.ace_editor.ace_multiselect
                                                                                                                            .ace_cursor {border - left - width: 1px;}.ace_marker-layer
                                                                                                                            .ace_step,
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_stack {position: absolute;z-index: 3;}.ace_marker-layer
                                                                                                                            .ace_selection {position: absolute;z-index: 5;}.ace_marker-layer
                                                                                                                            .ace_bracket {position: absolute;z-index: 6;}.ace_marker-layer
                                                                                                                            .ace_active-line {position: absolute;z-index: 2;}.ace_marker-layer
                                                                                                                            .ace_selected-word {position: absolute;z-index: 4;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;}.ace_line
                                                                                                                            .ace_fold {-moz - box - sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;display: inline-block;height: 11px;margin-top: -2px;vertical-align: middle;background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAJCAYAAADU6McMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJpJREFUeNpi/P//PwOlgAXGYGRklAVSokD8GmjwY1wasKljQpYACtpCFeADcHVQfQyMQAwzwAZI3wJKvCLkfKBaMSClBlR7BOQikCFGQEErIH0VqkabiGCAqwUadAzZJRxQr/0gwiXIal8zQQPnNVTgJ1TdawL0T5gBIP1MUJNhBv2HKoQHHjqNrA4WO4zY0glyNKLT2KIfIMAAQsdgGiXvgnYAAAAASUVORK5CYII="),url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAA3CAYAAADNNiA5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAACJJREFUeNpi+P//fxgTAwPDBxDxD078RSX+YeEyDFMCIMAAI3INmXiwf2YAAAAASUVORK5CYII=");background-repeat: no-repeat, repeat-x;background-position: center center, top left;color: transparent;border: 1px solid black;border-radius: 2px;cursor: pointer;pointer-events: auto;}.ace_dark
                                                                                                                            .ace_fold {}.ace_fold:hover{background - image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAJCAYAAADU6McMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAJpJREFUeNpi/P//PwOlgAXGYGRklAVSokD8GmjwY1wasKljQpYACtpCFeADcHVQfQyMQAwzwAZI3wJKvCLkfKBaMSClBlR7BOQikCFGQEErIH0VqkabiGCAqwUadAzZJRxQr/0gwiXIal8zQQPnNVTgJ1TdawL0T5gBIP1MUJNhBv2HKoQHHjqNrA4WO4zY0glyNKLT2KIfIMAAQsdgGiXvgnYAAAAASUVORK5CYII="),url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAA3CAYAAADNNiA5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAACBJREFUeNpi+P//fz4TAwPDZxDxD5X4i5fLMEwJgAADAEPVDbjNw87ZAAAAAElFTkSuQmCC");}.ace_tooltip {background - color: #FFF;background-image: -webkit-linear-gradient(top, transparent, rgba(0, 0, 0, 0.1));background-image: linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.1));border: 1px solid gray;border-radius: 1px;box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);color: black;max-width: 100%;padding: 3px 4px;position: fixed;z-index: 999999;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;cursor: default;white-space: pre;word-wrap: break-word;line-height: normal;font-style: normal;font-weight: normal;letter-spacing: normal;pointer-events: none;}.ace_folding-enabled
                                                                                                                            >
                                                                                                                            .ace_gutter-cell {padding - right: 13px;}.ace_fold-widget {-moz - box - sizing: border-box;-webkit-box-sizing: border-box;box-sizing: border-box;margin: 0 -12px 0 1px;display: none;width: 11px;vertical-align: top;background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAANElEQVR42mWKsQ0AMAzC8ixLlrzQjzmBiEjp0A6WwBCSPgKAXoLkqSot7nN3yMwR7pZ32NzpKkVoDBUxKAAAAABJRU5ErkJggg==");background-repeat: no-repeat;background-position: center;border-radius: 3px;border: 1px solid transparent;cursor: pointer;}.ace_folding-enabled
                                                                                                                            .ace_fold-widget {display: inline-block;}.ace_fold-widget.ace_end {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAANElEQVR42m3HwQkAMAhD0YzsRchFKI7sAikeWkrxwScEB0nh5e7KTPWimZki4tYfVbX+MNl4pyZXejUO1QAAAABJRU5ErkJggg==");}.ace_fold-widget.ace_closed {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAGCAYAAAAG5SQMAAAAOUlEQVR42jXKwQkAMAgDwKwqKD4EwQ26sSOkVWjgIIHAzPiCgaqiqnJHZnKICBERHN194O5b9vbLuAVRL+l0YWnZAAAAAElFTkSuQmCCXA==");}.ace_fold-widget:hover {border: 1px solid rgba(0, 0, 0, 0.3);background-color: rgba(255, 255, 255, 0.2);box-shadow: 0 1px 1px rgba(255, 255, 255, 0.7);}.ace_fold-widget:active {border: 1px solid rgba(0, 0, 0, 0.4);background-color: rgba(0, 0, 0, 0.05);box-shadow: 0 1px 1px rgba(255, 255, 255, 0.8);}.ace_dark
                                                                                                                            .ace_fold-widget {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHklEQVQIW2P4//8/AzoGEQ7oGCaLLAhWiSwB146BAQCSTPYocqT0AAAAAElFTkSuQmCC");}.ace_dark
                                                                                                                            .ace_fold-widget.ace_end {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAH0lEQVQIW2P4//8/AxQ7wNjIAjDMgC4AxjCVKBirIAAF0kz2rlhxpAAAAABJRU5ErkJggg==");}.ace_dark
                                                                                                                            .ace_fold-widget.ace_closed {background - image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAFCAYAAACAcVaiAAAAHElEQVQIW2P4//+/AxAzgDADlOOAznHAKgPWAwARji8UIDTfQQAAAABJRU5ErkJggg==");}.ace_dark
                                                                                                                            .ace_fold-widget:hover {box - shadow: 0 1px 1px rgba(255, 255, 255, 0.2);background-color: rgba(255, 255, 255, 0.1);}.ace_dark
                                                                                                                            .ace_fold-widget:active {box - shadow: 0 1px 1px rgba(255, 255, 255, 0.2);}.ace_fold-widget.ace_invalid {background - color: #FFB4B4;border-color: #DE5555;}.ace_fade-fold-widgets
                                                                                                                            .ace_fold-widget {-webkit - transition: opacity 0.4s ease 0.05s;transition: opacity 0.4s ease 0.05s;opacity: 0;}.ace_fade-fold-widgets:hover
                                                                                                                            .ace_fold-widget {-webkit - transition: opacity 0.05s ease 0.05s;transition: opacity 0.05s ease 0.05s;opacity:1;}.ace_underline {text - decoration: underline;}.ace_bold {font - weight: bold;}.ace_nobold
                                                                                                                            .ace_bold {font - weight: normal;}.ace_italic {font - style: italic;}.ace_error-marker {background - color: rgba(255, 0, 0,0.2);position: absolute;z-index: 9;}.ace_highlight-marker {background - color: rgba(255, 255, 0,0.2);position: absolute;z-index: 8;}.ace_br1 {border - top - left - radius    : 3px;}.ace_br2 {border - top - right - radius   : 3px;}.ace_br3 {border - top - left - radius    : 3px; border-top-right-radius:    3px;}.ace_br4 {border - bottom - right - radius: 3px;}.ace_br5 {border - top - left - radius    : 3px; border-bottom-right-radius: 3px;}.ace_br6 {border - top - right - radius   : 3px; border-bottom-right-radius: 3px;}.ace_br7 {border - top - left - radius    : 3px; border-top-right-radius:    3px; border-bottom-right-radius: 3px;}.ace_br8 {border - bottom - left - radius : 3px;}.ace_br9 {border - top - left - radius    : 3px; border-bottom-left-radius:  3px;}.ace_br10{border - top - right - radius   : 3px; border-bottom-left-radius:  3px;}.ace_br11{border - top - left - radius    : 3px; border-top-right-radius:    3px; border-bottom-left-radius:  3px;}.ace_br12{border - bottom - right - radius: 3px; border-bottom-left-radius:  3px;}.ace_br13{border - top - left - radius    : 3px; border-bottom-right-radius: 3px; border-bottom-left-radius:  3px;}.ace_br14{border - top - right - radius   : 3px; border-bottom-right-radius: 3px; border-bottom-left-radius:  3px;}.ace_br15{border - top - left - radius    : 3px; border-top-right-radius:    3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px;}
                                                                                                                            /*# sourceURL=ace/css/ace_editor.css */</style>
                                                                                                                        <style
                                                                                                                            id="ace-tm">.ace-tm
                                                                                                                            .ace_gutter {background: #f0f0f0;color: #333;}.ace-tm
                                                                                                                            .ace_print-margin {width: 1px;background: #e8e8e8;}.ace-tm
                                                                                                                            .ace_fold {background - color: #6B72E6;}.ace-tm {background - color: #FFFFFF;color: black;}.ace-tm
                                                                                                                            .ace_cursor {color: black;}.ace-tm
                                                                                                                            .ace_invisible {color: rgb(191, 191, 191);}.ace-tm
                                                                                                                            .ace_storage,.ace-tm
                                                                                                                            .ace_keyword {color: blue;}.ace-tm
                                                                                                                            .ace_constant {color: rgb(197, 6, 11);}.ace-tm
                                                                                                                            .ace_constant.ace_buildin {color: rgb(88, 72, 246);}.ace-tm
                                                                                                                            .ace_constant.ace_language {color: rgb(88, 92, 246);}.ace-tm
                                                                                                                            .ace_constant.ace_library {color: rgb(6, 150, 14);}.ace-tm
                                                                                                                            .ace_invalid {background - color: rgba(255, 0, 0, 0.1);color: red;}.ace-tm
                                                                                                                            .ace_support.ace_function {color: rgb(60, 76, 114);}.ace-tm
                                                                                                                            .ace_support.ace_constant {color: rgb(6, 150, 14);}.ace-tm
                                                                                                                            .ace_support.ace_type,.ace-tm
                                                                                                                            .ace_support.ace_class {color: rgb(109, 121, 222);}.ace-tm
                                                                                                                            .ace_keyword.ace_operator {color: rgb(104, 118, 135);}.ace-tm
                                                                                                                            .ace_string {color: rgb(3, 106, 7);}.ace-tm
                                                                                                                            .ace_comment {color: rgb(76, 136, 107);}.ace-tm
                                                                                                                            .ace_comment.ace_doc {color: rgb(0, 102, 255);}.ace-tm
                                                                                                                            .ace_comment.ace_doc.ace_tag {color: rgb(128, 159, 191);}.ace-tm
                                                                                                                            .ace_constant.ace_numeric {color: rgb(0, 0, 205);}.ace-tm
                                                                                                                            .ace_variable {color: rgb(49, 132, 149);}.ace-tm
                                                                                                                            .ace_xml-pe {color: rgb(104, 104, 91);}.ace-tm
                                                                                                                            .ace_entity.ace_name.ace_function {color: #0000A2;}.ace-tm
                                                                                                                            .ace_heading {color: rgb(12, 7, 255);}.ace-tm
                                                                                                                            .ace_list {color:rgb(185, 6, 144);}.ace-tm
                                                                                                                            .ace_meta.ace_tag {color:rgb(0, 22, 142);}.ace-tm
                                                                                                                            .ace_string.ace_regex {color: rgb(255, 0, 0)}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_selection {background: rgb(181, 213, 255);}.ace-tm.ace_multiselect
                                                                                                                            .ace_selection.ace_start {box - shadow: 0 0 3px 0px white;}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_step {background: rgb(252, 255, 0);}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_stack {background: rgb(164, 229, 101);}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_bracket {margin: -1px 0 0 -1px;border: 1px solid rgb(192, 192, 192);}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_active-line {background: rgba(0, 0, 0, 0.07);}.ace-tm
                                                                                                                            .ace_gutter-active-line {background - color : #dcdcdc;}.ace-tm
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_selected-word {background: rgb(250, 250, 255);border: 1px solid rgb(200, 200, 250);}.ace-tm
                                                                                                                            .ace_indent-guide {background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAE0lEQVQImWP4////f4bLly//BwAmVgd1/w11/gAAAABJRU5ErkJggg==") right repeat-y;}
                                                                                                                            /*# sourceURL=ace/css/ace-tm */</style>
                                                                                                                        <style> .error_widget_wrapper {background: inherit;        color: inherit;        border:none} .error_widget {border - top: solid 2px;        border-bottom: solid 2px;        margin: 5px 0;        padding: 10px 40px;        white-space: pre-wrap;} .error_widget.ace_error,
                                                                                                                            .error_widget_arrow.ace_error{border - color: #ff5a5a} .error_widget.ace_warning,
                                                                                                                            .error_widget_arrow.ace_warning{border - color: #F1D817} .error_widget.ace_info,
                                                                                                                            .error_widget_arrow.ace_info{border - color: #5a5a5a} .error_widget.ace_ok,
                                                                                                                            .error_widget_arrow.ace_ok{border - color: #5aaa5a} .error_widget_arrow {position: absolute;        border: solid 5px;        border-top-color: transparent!important;        border-right-color: transparent!important;        border-left-color: transparent!important;        top: -5px;}</style>
                                                                                                                        <style>.ace_snippet-marker {-moz - box - sizing: border-box;    box-sizing: border-box;    background: rgba(194, 193, 208, 0.09);    border: 1px dotted rgba(211, 208, 235, 0.62);    position: absolute;}</style>
                                                                                                                        <style>.ace_editor.ace_autocomplete
                                                                                                                            .ace_marker-layer
                                                                                                                            .ace_active-line {background - color: #CAD6FA;    z-index: 1;}.ace_editor.ace_autocomplete
                                                                                                                            .ace_line-hover {border: 1px solid #abbffe;    margin-top: -1px;    background: rgba(233,233,253,0.4);}.ace_editor.ace_autocomplete
                                                                                                                            .ace_line-hover {position: absolute;    z-index: 2;}.ace_editor.ace_autocomplete
                                                                                                                            .ace_scroller {background: none;   border: none;   box-shadow: none;}.ace_rightAlignedText {color: gray;    display: inline-block;    position: absolute;    right: 4px;    text-align: right;    z-index: -1;}.ace_editor.ace_autocomplete
                                                                                                                            .ace_completion-highlight{color: #000;    text-shadow: 0 0 0.01em;}.ace_editor.ace_autocomplete {width: 280px;    z-index: 200000;    background: #fbfbfb;    color: #444;    border: 1px lightgray solid;    position: fixed;    box-shadow: 2px 3px 5px rgba(0,0,0,.2);    line-height: 1.4;}</style>
                                                                                                                        <style
                                                                                                                            id="ace_searchbox">.ace_search {background - color: #ddd;color: #666;border: 1px solid #cbcbcb;border-top: 0 none;overflow: hidden;margin: 0;padding: 4px 6px 0 4px;position: absolute;top: 0;z-index: 99;white-space: normal;}.ace_search.left {border - left: 0 none;border-radius: 0px 0px 5px 0px;left: 0;}.ace_search.right {border - radius: 0px 0px 0px 5px;border-right: 0 none;right: 0;}.ace_search_form,
                                                                                                                            .ace_replace_form {margin: 0 20px 4px 0;overflow: hidden;line-height: 1.9;}.ace_replace_form {margin - right: 0;}.ace_search_form.ace_nomatch {outline: 1px solid red;}.ace_search_field {border - radius: 3px 0 0 3px;background-color: white;color: black;border: 1px solid #cbcbcb;border-right: 0 none;box-sizing: border-box!important;outline: 0;padding: 0;font-size: inherit;margin: 0;line-height: inherit;padding: 0 6px;min-width: 17em;vertical-align: top;}.ace_searchbtn {border: 1px solid #cbcbcb;line-height: inherit;display: inline-block;padding: 0 6px;background: #fff;border-right: 0 none;border-left: 1px solid #dcdcdc;cursor: pointer;margin: 0;position: relative;box-sizing: content-box!important;color: #666;}.ace_searchbtn:last-child {border - radius: 0 3px 3px 0;border-right: 1px solid #cbcbcb;}.ace_searchbtn:disabled {background: none;cursor: default;}.ace_searchbtn:hover {background - color: #eef1f6;}.ace_searchbtn.prev,
                                                                                                                            .ace_searchbtn.next {padding: 0px 0.7em}.ace_searchbtn.prev:after,
                                                                                                                            .ace_searchbtn.next:after {content: "";border: solid 2px #888;width: 0.5em;height: 0.5em;border-width:  2px 0 0 2px;display:inline-block;transform: rotate(-45deg);}.ace_searchbtn.next:after {border - width: 0 2px 2px 0 ;}.ace_searchbtn_close {background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAcCAYAAABRVo5BAAAAZ0lEQVR42u2SUQrAMAhDvazn8OjZBilCkYVVxiis8H4CT0VrAJb4WHT3C5xU2a2IQZXJjiQIRMdkEoJ5Q2yMqpfDIo+XY4k6h+YXOyKqTIj5REaxloNAd0xiKmAtsTHqW8sR2W5f7gCu5nWFUpVjZwAAAABJRU5ErkJggg==) no-repeat 50% 0;border-radius: 50%;border: 0 none;color: #656565;cursor: pointer;font: 16px/16px Arial;padding: 0;height: 14px;width: 14px;top: 9px;right: 7px;position: absolute;}.ace_searchbtn_close:hover {background - color: #656565;background-position: 50% 100%;color: white;}.ace_button {margin - left: 2px;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;-o-user-select: none;-ms-user-select: none;user-select: none;overflow: hidden;opacity: 0.7;border: 1px solid rgba(100,100,100,0.23);padding: 1px;box-sizing:    border-box!important;color: black;}.ace_button:hover {background - color: #eee;opacity:1;}.ace_button:active {background - color: #ddd;}.ace_button.checked {border - color: #3399ff;opacity:1;}.ace_search_options{margin - bottom: 3px;text-align: right;-webkit-user-select: none;-moz-user-select: none;-o-user-select: none;-ms-user-select: none;user-select: none;clear: both;}.ace_search_counter {float: left;font-family: arial;padding: 0 8px;}
                                                                                                                            /*# sourceURL=ace/css/ace_searchbox */</style>
                                                                                                                        <script
                                                                                                                            src="https://koolkanya.com/blogs/wp-content/plugins/td-composer/css-live/assets/external/ace/mode-less.js"></script>
                                                                                                                        <script
                                                                                                                            src="https://koolkanya.com/blogs/wp-content/plugins/td-composer/css-live/assets/external/ace/snippets/text.js"></script>
                                                                                                                        <style
                                                                                                                            type="text/css">.ub-emb-iframe-wrapper{display:none;position:relative;vertical-align:middle}.ub-emb-iframe-wrapper.ub-emb-visible{display:inline-block}.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-close{background - color:hsla(0,0%,100%,.6);border:0;border-radius:50%;color:#525151;cursor:pointer;font-family:Arial,sans-serif;font-size:20px;font-weight:400;height:20px;line-height:1;outline:none;padding:0;position:absolute;right:10px;text-align:center;top:10px;transition:transform .2s ease-in-out,background-color .2s ease-in-out;width:20px;z-index:1}.ub-emb-iframe-wrapper.ub-emb-mobile
                                                                                                                            .ub-emb-close{transition:none}.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-close:before{content:"";height:40px;position:absolute;right:-10px;top:-10px;width:40px}.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-close:hover{-ms - transform:scale(1.2);background-color:#fff;transform:scale(1.2)}.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-close:active,.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-close:focus{background:hsla(0,0%,100%,.35);color:#444;outline:none}.ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-iframe{border:0;max-height:100%;max-width:100%}.ub-emb-overlay
                                                                                                                            .ub-emb-iframe-wrapper
                                                                                                                            .ub-emb-iframe{box - shadow:0 0 12px rgba(0,0,0,.3),0 1px 5px rgba(0,0,0,.2)}.ub-emb-overlay
                                                                                                                            .ub-emb-iframe-wrapper.ub-emb-mobile{max - width:100vw}</style>
                                                                                                                        <style
                                                                                                                            type="text/css">.ub-emb-overlay{transition:visibility .3s step-end;visibility:hidden}.ub-emb-overlay.ub-emb-visible{transition:none;visibility:visible}.ub-emb-overlay
                                                                                                                            .ub-emb-backdrop,.ub-emb-overlay
                                                                                                                            .ub-emb-scroll-wrapper{opacity:0;position:fixed;transition:opacity .3s ease,z-index .3s step-end;z-index:-1}.ub-emb-overlay
                                                                                                                            .ub-emb-backdrop{background:rgba(0,0,0,.6);bottom:-1000px;left:-1000px;right:-1000px;top:-1000px}.ub-emb-overlay
                                                                                                                            .ub-emb-scroll-wrapper{-webkit - overflow - scrolling:touch;bottom:0;box-sizing:border-box;left:0;overflow:auto;padding:30px;right:0;text-align:center;top:0;white-space:nowrap;width:100%}.ub-emb-overlay
                                                                                                                            .ub-emb-scroll-wrapper:before{content:"";display:inline-block;height:100%;vertical-align:middle}.ub-emb-overlay.ub-emb-mobile
                                                                                                                            .ub-emb-scroll-wrapper{padding:30px 0}.ub-emb-overlay.ub-emb-visible
                                                                                                                            .ub-emb-backdrop,.ub-emb-overlay.ub-emb-visible
                                                                                                                            .ub-emb-scroll-wrapper{opacity:1;transition:opacity .4s ease;z-index:2147483647}</style>
                                                                                                                        <style
                                                                                                                            type="text/css">.ub-emb-bar{transition:visibility .2s step-end;visibility:hidden}.ub-emb-bar.ub-emb-visible{transition:none;visibility:visible}.ub-emb-bar
                                                                                                                            .ub-emb-bar-frame{left:0;position:fixed;right:0;text-align:center;transition:bottom .2s ease-in-out,top .2s ease-in-out,z-index .2s step-end;z-index:-1}.ub-emb-bar.ub-emb-ios
                                                                                                                            .ub-emb-bar-frame{right:auto;width:100%}.ub-emb-bar.ub-emb-visible
                                                                                                                            .ub-emb-bar-frame{transition:bottom .3s ease-in-out,top .3s ease-in-out;z-index:2147483646}.ub-emb-bar
                                                                                                                            .ub-emb-close{bottom:0;margin:auto 0;top:0}.ub-emb-bar:not(.ub-emb-mobile)
                                                                                                                            .ub-emb-close{right:20px}</style>
                                                                                                                        <script
                                                                                                                            src="https://koolkanya.com/blogs/wp-content/plugins/td-composer/css-live/assets/external/ace/snippets/less.js"></script>
                                                                                                                        <script
                                                                                                                            async=""
                                                                                                                            src="https://script.hotjar.com/modules.fee7048ea23070895b33.js"
                                                                                                                            charSet="utf-8"></script>
                                                                                                                        <style
                                                                                                                            type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style>
</head>
<body>`


export default KKHtml;
