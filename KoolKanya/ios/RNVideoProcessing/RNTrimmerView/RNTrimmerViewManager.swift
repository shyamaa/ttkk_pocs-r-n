//
//  RNTrimmerViewManager.swift
//  KoolKanya
//
//  Created by Mobcoder on 30/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

@objc(RNTrimmerViewManager)
class RNTrimmerViewManager: RCTViewManager {
  
  @objc override func view() -> UIView! {
    return RNTrimmerView(frame: CGRect.zero, bridge: self.bridge)
  }
}
