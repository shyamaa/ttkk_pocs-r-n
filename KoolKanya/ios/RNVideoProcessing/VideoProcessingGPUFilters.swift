//
//  VideoProcessingGPUFilters.swift
//  KoolKanya
//
//  Created by Mobcoder on 30/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import GPUImage

class VideoProcessingGPUFilters {
  let filters = [
    "saturation": GPUImageSaturationFilter(),
    "sepia": GPUImageSepiaFilter(),
    "pixelate": GPUImagePixellateFilter(),
    "hue": GPUImageHueFilter(),
    "vignette": GPUImageVignetteFilter(),
    "guassianBlur": GPUImageGaussianBlurFilter()
  ]
  
  // TODO: add more filters
  
  func getFilterByName(name: String) -> GPUImageFilter? {
    return filters[name]
  }
  
  func getAllFilters() -> [String: GPUImageFilter] {
    return filters
  }
}
