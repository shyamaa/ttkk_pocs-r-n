//
//  RNVideoProcessingManager.swift
//  KoolKanya
//
//  Created by Mobcoder on 30/08/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

@objc(RNVideoProcessingManager)
class RNVideoProcessingManager: RCTViewManager {
  
  @objc override func view() -> UIView! {
    return RNVideoPlayer()
  }
  
  @objc override func constantsToExport() -> [AnyHashable: Any] {
    return [
      "ScaleNone": AVLayerVideoGravity.resizeAspect,
      "ScaleToFill": AVLayerVideoGravity.resize,
      "ScaleAspectFit": AVLayerVideoGravity.resizeAspect,
      "ScaleAspectFill": AVLayerVideoGravity.resizeAspectFill
    ]
  }
  
  override class func requiresMainQueueSetup() -> Bool {
    return true
  }
}
